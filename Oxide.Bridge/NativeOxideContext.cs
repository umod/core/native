﻿using System;

using Oxide.Core;
using Oxide.Core.Logging;

namespace Oxide.Bridge
{
    /// <summary>
    /// Encapsulates the Oxide API into a native-friendly object
    /// </summary>
    public class NativeOxideContext : IOxideContext
    {
        private Exception cachedException;

        private object[] pendingArgs;

        private DebugCallback debugCallback;

        /// <summary>
        /// Sets a debug callback
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public ErrorCode SetDebugCallback(DebugCallback callback)
        {
            debugCallback = callback;
            Interface.DebugCallback = DebugCallback;
            return ErrorCode.OK;
        }

        private void DebugCallback(string message) => debugCallback?.Invoke(message);

        /// <summary>
        /// Initializes the Oxide mod
        /// </summary>
        public ErrorCode Initialize()
        {
            try
            {
                Interface.Initialize();
            }
            catch (Exception ex)
            {
                cachedException = ex;
                return ErrorCode.ManagedException;
            }
            return ErrorCode.OK;
        }

        /// <summary>
        /// Starts calling a hook with the specified arg count
        /// </summary>
        /// <param name="argCount"></param>
        /// <returns></returns>
        public ErrorCode StartCallHook(int argCount)
        {
            if (argCount < 0 || argCount > 16)
            {
                cachedException = new Exception("Invalid argument count");
                return ErrorCode.ManagedException;
            }
            pendingArgs = new object[argCount];
            return ErrorCode.OK;
        }

        /// <summary>
        /// Sets the hook argument at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ErrorCode SetCallHookArg(int index, object value)
        {
            try
            {
                pendingArgs[index] = value;
                Console.WriteLine("Setting arg {0} to '{1}' ({2})", index, value, value?.GetType()?.Name);
            }
            catch (Exception ex)
            {
                cachedException = ex;
                return ErrorCode.ManagedException;
            }
            return ErrorCode.OK;
        }

        /// <summary>
        /// Calls a hook
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="returnValue"></param>
        /// <returns></returns>
        public ErrorCode CallHook(string hookName, out object returnValue)
        {
            try
            {
                returnValue = pendingArgs != null ? "Oxide says hey" : Interface.CallHook(hookName, pendingArgs);
                pendingArgs = null;
            }
            catch (Exception ex)
            {
                cachedException = ex;
                returnValue = null;
                return ErrorCode.ManagedException;
            }
            return ErrorCode.OK;
        }

        /// <summary>
        /// Writes a message to the logging system
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public ErrorCode WriteLog(LogType logType, string message)
        {
            try
            {
                Interface.Oxide.RootLogger.Write(logType, "{0}", message);
            }
            catch (Exception ex)
            {
                cachedException = ex;
                return ErrorCode.ManagedException;
            }
            return ErrorCode.OK;
        }

        /// <summary>
        /// Gets the exception if an ErrorCode from a previous call was flagged as ManagedException
        /// </summary>
        /// <returns></returns>
        public string GetException()
        {
            var ex = cachedException;
            cachedException = null;
            return ex.ToString();
        }
    }
}
