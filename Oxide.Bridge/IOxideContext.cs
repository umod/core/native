﻿using System.Runtime.InteropServices;

using Oxide.Core.Logging;

namespace Oxide.Bridge
{
    /// <summary>
    /// Represents possible error codes
    /// </summary>
    public enum ErrorCode
    {
        Unknown = 0x00,
        OK = 0x01,
        ManagedException = 0x02
    }

    public delegate void DebugCallback([MarshalAs(UnmanagedType.LPStr)] string message);

    /// <summary>
    /// Represents a context in which Oxide functions are executed
    /// </summary>
    [ComVisible(true)]
    [Guid("FD814314-8E66-4FA6-A9BB-682F736CAF5E")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOxideContext
    {
        /// <summary>
        /// Sets a debug callback
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        ErrorCode SetDebugCallback(DebugCallback callback);

        /// <summary>
        /// Initializes the Oxide mod
        /// </summary>
        ErrorCode Initialize();

        /// <summary>
        /// Starts calling a hook with the specified arg count
        /// </summary>
        /// <param name="argCount"></param>
        /// <returns></returns>
        ErrorCode StartCallHook(int argCount);

        /// <summary>
        /// Sets the hook argument at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        ErrorCode SetCallHookArg(int index, object value);

        /// <summary>
        /// Calls a hook
        /// </summary>
        /// <param name="hookName"></param>
        /// <param name="returnValue"></param>
        /// <returns></returns>
        ErrorCode CallHook(string hookName, out object returnValue);

        /// <summary>
        /// Writes a message to the logging system
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        ErrorCode WriteLog(LogType logType, string message);

        /// <summary>
        /// Gets the exception if an ErrorCode from a previous call was flagged as ManagedException
        /// </summary>
        /// <returns></returns>
        string GetException();
    }
}
