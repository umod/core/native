﻿using System.Runtime.InteropServices;

using Oxide.Core.Logging;

using RGiesecke.DllExport;

namespace Oxide.Bridge
{
    /// <summary>
    /// Contains entry points for native code
    /// </summary>
    public static class Exports
    {
        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_CreateContext")]
        public static void CreateContext([MarshalAs(UnmanagedType.Interface)] out IOxideContext context)
        {
            context = new NativeOxideContext();
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_SetDebugCallback")]
        public static ErrorCode Oxide_SetDebugCallback([MarshalAs(UnmanagedType.Interface)] IOxideContext context, [MarshalAs(UnmanagedType.FunctionPtr)] DebugCallback callback)
        {
            return context.SetDebugCallback(callback);
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_Initialize")]
        public static ErrorCode Oxide_Initialize([MarshalAs(UnmanagedType.Interface)] IOxideContext context)
        {
            return context.Initialize();
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_StartCallHook")]
        public static ErrorCode Oxide_StartCallHook(IOxideContext context, int argCount)
        {
            return context.StartCallHook(argCount);
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_SetCallHookArg")]
        public static ErrorCode Oxide_SetCallHookArg(IOxideContext context, int index, object value)
        {
            return context.SetCallHookArg(index, value);
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_CallHook")]
        public static ErrorCode Oxide_CallHook(IOxideContext context, string hookName, out object returnValue)
        {
            return context.CallHook(hookName, out returnValue);
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_WriteLog")]
        public static ErrorCode Oxide_WriteLog(IOxideContext context, LogType logType, string message)
        {
            return context.WriteLog(logType, message);
        }

        [DllExport(CallingConvention = CallingConvention.Cdecl, ExportName = "Oxide_GetException")]
        public static string Oxide_GetException(IOxideContext context)
        {
            return context.GetException();
        }
    }
}
