﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Oxide.SymbolMapParser
{
    class Program
    {
        private const int TABLE_SIZE = 36;
        private const int VALUE_SIZE = 24;

        private struct Value
        {
            public readonly string Key;
            public readonly ulong RVA;

            public Value(string key, ulong rva)
            {
                Key = key;
                RVA = rva;
            }
        }

        static string FromByteArray(byte[] arr, int idx)
        {
            var chars = new List<char>();
            var c = (char)arr[idx];
            while (c != '\0')
            {
                chars.Add(c);
                c = (char)arr[++idx];
            }
            return new string(chars.ToArray());
        }

        static void Main(string[] args)
        {
            var values = new List<Value>();

            using (var strm = File.OpenRead(args[0]))
            {
                using (var rdr = new BinaryReader(strm))
                {
                    if (rdr.ReadChar() != 'S' || rdr.ReadChar() != 'T' || rdr.ReadChar() != 'R' || rdr.ReadChar() != 'M')
                        throw new Exception("Invalid header");
                    var tableCount = rdr.ReadInt32();
                    var valueCount = rdr.ReadInt32();
                    var charCount = rdr.ReadInt32();
                    //ulong sig = rdr.ReadUInt64();

                    strm.Seek(-charCount, SeekOrigin.End);

                    var chars = rdr.ReadBytes(charCount);

                    strm.Seek(24 + tableCount * TABLE_SIZE, SeekOrigin.Begin);

                    for (var i = 0; i < valueCount; i++)
                    {
                        var strOff = rdr.ReadInt32();
                        //int unused1 = rdr.ReadInt32();
                        //int unused2 = rdr.ReadInt32();
                        //int unused3 = rdr.ReadInt32();
                        var rva = rdr.ReadInt64();
                        values.Add(new Value(FromByteArray(chars, strOff), (ulong)rva));
                    }

                    values.Sort((a, b) => Comparer<string>.Default.Compare(a.Key, b.Key));

                    Console.WriteLine($"We loaded {values.Count} symbols!");
                }
            }

            using (var strm = File.OpenWrite("symbol_map.txt"))
            {
                using (var wtr = new StreamWriter(strm))
                {
                    for (var i = 0; i < values.Count; i++)
                    {
                        var value = values[i];
                        wtr.WriteLine($"0x{value.RVA:X} {value.Key}");
                    }
                    wtr.Flush();
                }
            }

            Console.ReadKey();
            Console.WriteLine("Done");
        }
    }
}
