﻿namespace Oxide.Native.ManagedShell
{
    /// <summary>
    /// Represents a possible taregt
    /// </summary>
    public class TargetManifest
    {
        /// <summary>
        /// Gets the name of the game
        /// </summary>
        public string GameName { get; set; }

        /// <summary>
        /// Gets the name of the executable to run
        /// </summary>
        public string ExecutableName { get; set; }

        /// <summary>
        /// Gets the name of the dll to inject
        /// </summary>
        public string InjectDllName { get; set; }
    }
}
