﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Runtime.InteropServices;
using System.Text;

namespace Oxide.Native.ManagedShell
{
    public enum LogType
    {
        Debug,
        Info,
        Warning,
        Error
    }

    public enum PacketType
    {
        LogMessage = 0x01,
        InjectionComplete = 0x02
    }

    /// <summary>
    /// Responsible for process and injection life-cycle
    /// </summary>
    public class ManagedShell
    {
        #region Singleton

        /// <summary>
        /// Gets the singleton instance
        /// </summary>
        public static ManagedShell Instance { get; private set; }

        /// <summary>
        /// Initializes the singleton
        /// </summary>
        public static void Initialize()
        {
            Instance = new ManagedShell();
        }

        #endregion

        // All possible manifests
        private HashSet<TargetManifest> manifests;

        // The process
        private Process process;

        // Pipe things
        private volatile NamedPipeServerStream pipeServer;
        private byte[] pipeBuffer;
        private const int PIPE_BUFFER_SIZE = 0x1000;

        // Access control
        private object syncRoot;

        /// <summary>
        /// Initializes a new instance of the ManagedShell class
        /// </summary>
        private ManagedShell()
        {
            // Initialize sync
            syncRoot = new object();

            // Initialize manifests
            manifests = new HashSet<TargetManifest>();
            LoadManifests();
        }

        #region Manifests

        /// <summary>
        /// Loads all manifests in
        /// </summary>
        private void LoadManifests()
        {
            // ARK: Survival Evolved
            manifests.Add(new TargetManifest
            {
                GameName = "ARK: Survival Evolved",
                ExecutableName = "ShooterGameServer.exe",
                InjectDllName = "Oxide.Native.ARK.dll"
            });
        }

        /// <summary>
        /// Finds the manifest to use
        /// </summary>
        /// <returns></returns>
        private TargetManifest FindManifest()
        {
            // Loop each manifest
            foreach (var manifest in manifests)
            {
                // Create full path
                var fullPath = Path.GetFullPath(manifest.ExecutableName);
                if (File.Exists(fullPath))
                {
                    return manifest;
                }
            }

            // Not found
            return null;
        }

        #endregion

        #region Logging

        /// <summary>
        /// Logs the specified message
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        /// <param name="piped"></param>
        public void WriteLog(LogType logType, string message, bool piped = false)
        {
            lock (syncRoot)
            {
                switch (logType)
                {
                    case LogType.Info:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("[INFO] ");
                        break;
                    case LogType.Warning:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write("[WARN] ");
                        break;
                    case LogType.Error:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("[ERROR] ");
                        break;
                    case LogType.Debug:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write("[DEBUG] ");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write("[Unknown] ");
                        break;
                }
                if (piped)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("[P] ");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(message);
            }
        }

        #endregion

        /// <summary>
        /// Runs the managed shell logic
        /// </summary>
        public void Run()
        {
            try
            {
                RunInternal();
            }
            catch (Exception ex)
            {
                WriteLog(LogType.Error, $"{ex}");
            }
        }

        /// <summary>
        /// Runs the actual managed shell logic
        /// </summary>
        private void RunInternal()
        {
            // Install ctrl handler
            SetConsoleCtrlHandler(ConsoleCtrlCheck, true);

            // Find manifest
            WriteLog(LogType.Info, "Searching for suitable target...");
            var manifest = FindManifest();
            if (manifest == null)
            {
                throw new Exception("Failed to find suitable target");
            }
            WriteLog(LogType.Info, $"Found game '{manifest.GameName}'");

            // Create the process
            WriteLog(LogType.Info, "Starting process...");
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = Path.GetFullPath(manifest.ExecutableName);
            startInfo.Arguments = Environment.CommandLine;
            var startupInfo = new StartupInfo();
            ProcessInformation procInfo;
            var cmdLineArgs = Environment.GetCommandLineArgs();
            var lessCmdLineArgs = new string[cmdLineArgs.Length - 1];
            Array.Copy(cmdLineArgs, 1, lessCmdLineArgs, 0, cmdLineArgs.Length - 1);
            string commandLine = $"\"{Path.GetFullPath(manifest.ExecutableName)}\" {string.Join(" ", lessCmdLineArgs)}";
            var unusedSecAttr = new SecurityAttributes();
            if (!CreateProcess(
                null,
                commandLine,
                ref unusedSecAttr,
                ref unusedSecAttr,
                true,
                CreationFlags.CREATE_SUSPENDED,
                IntPtr.Zero,
                Directory.GetCurrentDirectory(),
                ref startupInfo,
                out procInfo
                ))
            {
                WriteLog(LogType.Error, $"Failed to create process! ({GetLastError()})");
                return;
            }
            process = Process.GetProcessById(procInfo.processId);
            WriteLog(LogType.Info, $"Process ID is {process.Id}");

            // See if we can inject
            var dllPath = Path.GetFullPath(manifest.InjectDllName);
            if (File.Exists(dllPath))
            {
                // Setup pipe
                SetupPipe();

                // Inject
                PerformInjection(dllPath);
            }

            // Wait until it exits
            ResumeThreads();
            process.WaitForExit();
            process = null;

            // Clean up
            Terminate();
        }

        /// <summary>
        /// Suspends all threads in the child process
        /// </summary>
        private int SuspendThreads()
        {
            // Iterate each thread
            var cnt = 0;
            foreach (ProcessThread t in process.Threads)
            {
                var threadHdl = IntPtr.Zero;
                try
                {
                    // Get handle to it
                    threadHdl = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)t.Id);

                    // Verify it
                    if (threadHdl == IntPtr.Zero)
                    {
                        WriteLog(LogType.Warning, $"Failed to open thread handle ({GetLastError()})");
                    }
                    else
                    {
                        // Suspend
                        if (SuspendThread(threadHdl) != 0)
                        {
                            WriteLog(LogType.Warning, $"Failed to suspend thread ({GetLastError()})");
                        }
                        else
                            cnt++;
                    }
                }
                finally
                {
                    // Clean up
                    if (!CloseHandle(threadHdl))
                    {
                        WriteLog(LogType.Warning, $"Failed to close thread handle ({GetLastError()})");
                    }
                }
            }

            // Done
            WriteLog(LogType.Debug, $"Suspended {cnt} thread(s).");
            return cnt;
        }

        /// <summary>
        /// Resumes all threads in the child process
        /// </summary>
        private void ResumeThreads()
        {
            // Iterate each thread
            var cnt = 0;
            foreach (ProcessThread t in process.Threads)
            {
                var threadHdl = IntPtr.Zero;
                try
                {
                    // Get handle to it
                    threadHdl = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)t.Id);

                    // Verify it
                    if (threadHdl == IntPtr.Zero)
                    {
                        WriteLog(LogType.Warning, $"Failed to open thread handle ({GetLastError()})");
                    }
                    else
                    {
                        // Resume
                        if (ResumeThread(threadHdl) == -1)
                        {
                            WriteLog(LogType.Warning, $"Failed to resume thread ({GetLastError()})");
                        }
                        else
                            cnt++;
                    }
                }
                finally
                {
                    // Clean up
                    if (!CloseHandle(threadHdl))
                    {
                        WriteLog(LogType.Warning, $"Failed to close thread handle ({GetLastError()})");
                    }
                }
            }

            // Done
            WriteLog(LogType.Debug, $"Resumed {cnt} thread(s).");
        }

        /// <summary>
        /// Sets up the pipe server
        /// </summary>
        private void SetupPipe()
        {
            // Create the pipe server
            string pipeName = $"OxideNative_{process.Id}";
            pipeServer = new NamedPipeServerStream(pipeName, PipeDirection.In, 2, PipeTransmissionMode.Message, PipeOptions.Asynchronous, 1024, 1024);

            // Wait for a connection
            pipeServer.BeginWaitForConnection(Pipe_OnConnection, null);

            // Done
            WriteLog(LogType.Info, "Pipe is open.");
        }

        /// <summary>
        /// Called when the pipe received a connection
        /// </summary>
        /// <param name="result"></param>
        private void Pipe_OnConnection(IAsyncResult result)
        {
            // Accept connection
            try
            {
                pipeServer.EndWaitForConnection(result);
            }
            catch (ObjectDisposedException)
            {
                // Pipe closed
                pipeServer.Close();
                pipeServer = null;
                WriteLog(LogType.Info, "Pipe is closed.");
                return;
            }

            // Start reading data
            pipeBuffer = new byte[PIPE_BUFFER_SIZE];
            pipeServer.BeginRead(pipeBuffer, 0, PIPE_BUFFER_SIZE, Pipe_OnData, null);
        }

        /// <summary>
        /// Called when the pipe received data
        /// </summary>
        /// <param name="result"></param>
        private void Pipe_OnData(IAsyncResult result)
        {
            // End reading data
            try
            {
                var bytesRead = pipeServer.EndRead(result);
                if (bytesRead > 0) HandlePacket(pipeBuffer, bytesRead);
                pipeServer.BeginRead(pipeBuffer, 0, PIPE_BUFFER_SIZE, Pipe_OnData, null);
            }
            catch (ObjectDisposedException)
            {
                // Pipe closed
                pipeServer.Close();
                pipeServer = null;
                WriteLog(LogType.Info, "Pipe is closed.");
            }
            catch (Exception ex)
            {
                WriteLog(LogType.Warning, $"Got {ex} on pipe data callback");
            }
        }

        private void HandlePacket(byte[] data, int len)
        {
            // Read packet type
            var packetType = (PacketType)data[0];
            switch (packetType)
            {
                case PacketType.LogMessage:
                    // Read log type
                    var logType = (LogType)data[1];

                    // Parse message
                    var message = Encoding.ASCII.GetString(data, 2, len - 2);

                    // Log
                    WriteLog(logType, message, true);
                    break;
                default:
                    WriteLog(LogType.Warning, $"Unhandled packet type '{packetType}'", true);
                    break;
            }
        }

        /// <summary>
        /// Performs the injection process
        /// </summary>
        private void PerformInjection(string dllPath)
        {
            WriteLog(LogType.Info, "Injecting dll...");
            var processHdl = IntPtr.Zero;
            try
            {
                // Open the process handle
                processHdl = OpenProcess(process,
                    ProcessAccessFlags.CreateThread |
                    ProcessAccessFlags.QueryInformation |
                    ProcessAccessFlags.VirtualMemoryOperation |
                    ProcessAccessFlags.VirtualMemoryWrite |
                    ProcessAccessFlags.VirtualMemoryRead);
                if (processHdl == IntPtr.Zero)
                {
                    WriteLog(LogType.Error, $"Failed to open process handle ({GetLastError()})");
                    return;
                }

                // Allocate memory for the dll path
                var dllPathAddr = VirtualAllocEx(processHdl, IntPtr.Zero, (uint)(dllPath.Length + 1), AllocationType.Reserve | AllocationType.Commit, MemoryProtection.ExecuteReadWrite);
                if (dllPathAddr == IntPtr.Zero)
                {
                    WriteLog(LogType.Error, $"Failed to allocate memory in the process ({GetLastError()})");
                    return;
                }

                // Write the dll path
                var buffer = new byte[dllPath.Length + 1];
                Encoding.ASCII.GetBytes(dllPath, 0, dllPath.Length, buffer, 0);
                buffer[buffer.Length - 1] = 0;
                IntPtr written;
                if (!WriteProcessMemory(processHdl, dllPathAddr, buffer, buffer.Length, out written) || (int)written < buffer.Length)
                {
                    WriteLog(LogType.Error, $"Failed to write memory in the process ({GetLastError()})");
                    return;
                }

                // Get the address of LoadLibrary
                var loadLibAddr = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
                if (loadLibAddr == IntPtr.Zero)
                {
                    WriteLog(LogType.Error, $"Failed to get address of kernel32.dll!LoadLibraryA ({GetLastError()})");
                    return;
                }

                // Create the remote thread
                IntPtr threadId;
                var ptr = CreateRemoteThread(processHdl, IntPtr.Zero, 0, loadLibAddr, dllPathAddr, 0, out threadId);
                if (ptr == IntPtr.Zero)
                {
                    WriteLog(LogType.Error, $"Failed to create remote thread in process ({GetLastError()})");
                    return;
                }
                WriteLog(LogType.Debug, $"Remote thread ID is {threadId:H}");

                // Wait for it to complete
                WaitForSingleObject(ptr, 0xFFFFFFFF);
                WriteLog(LogType.Info, "Injection complete.");
            }
            finally
            {
                if (processHdl != IntPtr.Zero)
                {
                    // Close the handle
                    if (!CloseHandle(processHdl))
                    {
                        WriteLog(LogType.Error, $"Failed to close process handle ({GetLastError()})");
                    }
                }
            }
        }

        /// <summary>
        /// Terminates the managed shell cleanly
        /// </summary>
        private void Terminate()
        {
            // Log
            WriteLog(LogType.Info, "Shutting down...");

            // Close pipe
            pipeServer?.Close();

            // Is the process running?
            if (process != null)
            {
                // If it has a handle, try and close it via WM_CLOSE
                if (process.MainWindowHandle != IntPtr.Zero)
                {
                    SendMessage(process.MainWindowHandle, 0x0010, IntPtr.Zero, IntPtr.Zero); // WM_CLOSE
                }

                // Now terminate it
                WriteLog(LogType.Info, "Terminating process...");
                try
                {
                    process.Kill();
                }
                catch (Exception)
                {
                    WriteLog(LogType.Warning, "Got an exception trying to terminate the process but we don't care much.");
                }
                process = null;
            }
        }

        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            // Close event?
            switch (ctrlType)
            {
                case CtrlTypes.CTRL_CLOSE_EVENT:
                    // Shut down
                    Instance.Terminate();

                    break;

            }
            return true;
        }

        #region Unmanaged

        private enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }

        [Flags]
        private enum ThreadAccess : int
        {
            TERMINATE = (0x0001),
            SUSPEND_RESUME = (0x0002),
            GET_CONTEXT = (0x0008),
            SET_CONTEXT = (0x0010),
            SET_INFORMATION = (0x0020),
            QUERY_INFORMATION = (0x0040),
            SET_THREAD_TOKEN = (0x0080),
            IMPERSONATE = (0x0100),
            DIRECT_IMPERSONATION = (0x0200)
        }

        [Flags]
        private enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VirtualMemoryOperation = 0x00000008,
            VirtualMemoryRead = 0x00000010,
            VirtualMemoryWrite = 0x00000020,
            DuplicateHandle = 0x00000040,
            CreateProcess = 0x000000080,
            SetQuota = 0x00000100,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            QueryLimitedInformation = 0x00001000,
            Synchronize = 0x00100000
        }

        [Flags]
        private enum AllocationType
        {
            Commit = 0x1000,
            Reserve = 0x2000,
            Decommit = 0x4000,
            Release = 0x8000,
            Reset = 0x80000,
            Physical = 0x400000,
            TopDown = 0x100000,
            WriteWatch = 0x200000,
            LargePages = 0x20000000
        }

        [Flags]
        private enum MemoryProtection
        {
            Execute = 0x10,
            ExecuteRead = 0x20,
            ExecuteReadWrite = 0x40,
            ExecuteWriteCopy = 0x80,
            NoAccess = 0x01,
            ReadOnly = 0x02,
            ReadWrite = 0x04,
            WriteCopy = 0x08,
            GuardModifierflag = 0x100,
            NoCacheModifierflag = 0x200,
            WriteCombineModifierflag = 0x400
        }

        [Flags]
        private enum CreationFlags
        {
            CREATE_SUSPENDED = 0x00000004,
            CREATE_NEW_CONSOLE = 0x00000010,
            CREATE_NEW_PROCESS_GROUP = 0x00000200,
            CREATE_UNICODE_ENVIRONMENT = 0x00000400,
            CREATE_SEPARATE_WOW_VDM = 0x00000800,
            CREATE_DEFAULT_ERROR_MODE = 0x04000000,
        }

        [Flags]
        private enum LogonFlags
        {
            LOGON_WITH_PROFILE = 0x00000001,
            LOGON_NETCREDENTIALS_ONLY = 0x00000002
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct StartupInfo
        {
            public int cb;
            public string reserved;
            public string desktop;
            public string title;
            public int x;
            public int y;
            public int xSize;
            public int ySize;
            public int xCountChars;
            public int yCountChars;
            public int fillAttribute;
            public int flags;
            public ushort showWindow;
            public ushort reserved2;
            public byte reserved3;
            public IntPtr stdInput;
            public IntPtr stdOutput;
            public IntPtr stdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct ProcessInformation
        {
            public IntPtr process;
            public IntPtr thread;
            public int processId;
            public int threadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SecurityAttributes
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        private delegate bool HandlerRoutine(CtrlTypes CtrlType);

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int SuspendThread(IntPtr hThread);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int ResumeThread(IntPtr hThread);

        [DllImport("kernel32.dll")]
        private static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

        [DllImport("kernel32.dll")]
        private static extern IntPtr OpenProcess(ProcessAccessFlags processAccess, bool bInheritHandle, int processId);
        private static IntPtr OpenProcess(Process proc, ProcessAccessFlags flags) => OpenProcess(flags, false, proc.Id);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int nSize, out IntPtr lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll")]
        private static extern uint GetLastError();

        [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        //[DllImport("kernel32.dll")]
        //static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, ThreadStartDelegate lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);

        [DllImport("kernel32.dll")]
        private static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, out IntPtr lpThreadId);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern uint WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern bool CreateProcessWithLogonW(
             string userName,
             string domain,
             IntPtr password,
             LogonFlags logonFlags,
             string applicationName,
             string commandLine,
             CreationFlags creationFlags,
             uint environment,
             string currentDirectory,
             ref StartupInfo startupInfo,
             out ProcessInformation processInformation);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool CreateProcess(
            string lpApplicationName,
            string lpCommandLine,
            ref SecurityAttributes lpProcessAttributes,
            ref SecurityAttributes lpThreadAttributes,
            bool bInheritHandles,
            CreationFlags dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            [In] ref StartupInfo lpStartupInfo,
            out ProcessInformation lpProcessInformation);

        #endregion
    }
}
