﻿using System;

namespace Oxide.Native.ManagedShell
{
    class Program
    {
        static void Main(string[] args)
        {
            ManagedShell.Initialize();
            ManagedShell.Instance.Run();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
