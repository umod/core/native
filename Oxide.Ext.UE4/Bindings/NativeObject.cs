﻿using System;
using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings
{
    /// <summary>
    /// Represents an object that is backed by a native class instance
    /// </summary>
    public abstract class NativeObject : IEquatable<NativeObject>
    {
        // The native class pointer
        protected readonly NativePointer nativePtr;

        /// <summary>
        /// Initializes a new instance of the NativeObject class
        /// </summary>
        /// <param name="nativePtr"></param>
        protected NativeObject(NativePointer nativePtr)
        {
            // Store pointer
            this.nativePtr = nativePtr;
        }

        /// <summary>
        /// Acquires the specified function for this class
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="index"></param>
        /// <param name="shouldThrow"></param>
        /// <returns></returns>
        public static T AcquireFunction<T>(string signature, int index = 0, bool shouldThrow = true) where T : class
        {
            NativePointer ptr = Imports.OxideNative_ReflectSymbol(signature, index);
            if (ptr == NativePointer.Null)
            {
                if (shouldThrow)
                    throw new Exception($"Failed to locate native function by signature '{signature}'");
                return null;
            }
            return Marshal.GetDelegateForFunctionPointer(ptr, typeof(T)) as T;
        }

        /// <summary>
        /// Returns if this NativeObject represents the same native class as the other
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(NativeObject other) => nativePtr == other.nativePtr;

        /// <summary>
        /// Gets the raw data of this native object
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public byte[] GetRawData(int size)
        {
            var data = new byte[size];
            Marshal.Copy(nativePtr, data, 0, size);
            return data;
        }

        #region Allocator

        public static T Allocator<T>(NativePointer nativePtr) where T : class
        {
            // Check for null
            if (nativePtr.IsNull) return null;

            // Find the vftable
            //NativePointer vftable =
            NativePointer strPtr = Imports.OxideNative_ReverseReflectSymbol(nativePtr.Deref<NativePointer>());
            var vftableName = strPtr.ToNullTerminatedString();
            if (vftableName == null) throw new InvalidOperationException("Unable to determine vftable from object pointer (reverse symbol lookup failed)");

            const string postfix = "::`vftable'";
            if (vftableName.Length > postfix.Length && vftableName.Substring(vftableName.Length - postfix.Length) == postfix)
            {
                var classname = vftableName.Substring(0, vftableName.Length - postfix.Length);
                var theType = Type.GetType($"Oxide.Ext.UE4.Bindings.Generated.{classname}_impl", true); // TODO: Better way than this?
                return Activator.CreateInstance(theType, nativePtr) as T;
            }
            throw new InvalidOperationException($"Unable to determine vftable from object pointer (got non-vftable symbol '{vftableName}')");
        }

        #endregion

        public static implicit operator NativePointer(NativeObject obj) => obj.nativePtr;

        public override string ToString() => $"{GetType().Name} ({nativePtr})";

        #region Operator Overloads

        public static bool operator ==(NativeObject left, NativeObject right) => left.nativePtr == right.nativePtr;

        public static bool operator !=(NativeObject left, NativeObject right) => left.nativePtr != right.nativePtr;

        public override bool Equals(object obj)
        {
            var other = obj as NativeObject;
            if (other == null) return false;
            return nativePtr == other.nativePtr;
        }

        public override int GetHashCode() => nativePtr.GetHashCode();

        #endregion
    }
}
