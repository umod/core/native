﻿using System;
using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings
{
    /// <summary>
    /// Utilities for FName
    /// </summary>
    public static class FName
    {
        public enum EFindName
        {
            FNAME_Find = 0x0,
            FNAME_Add = 0x1,
            FNAME_Replace_Not_Safe_For_Threading = 0x2
        }

        #region Symbols

        [StructLayout(LayoutKind.Sequential, Size = Generated.Implementations.FName_impl.SIZE)]
        private struct Temp_FName
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = Generated.Implementations.FName_impl.SIZE)]
            public byte[] Data;

            public Temp_FName(byte[] data)
            {
                Data = data;
            }
        }

        private delegate void _FName_Init(ref Temp_FName fname, [MarshalAs(UnmanagedType.LPWStr)] string InName, int InNumber, EFindName FindType, bool bSplitName, int HardcodeIndex);
        private static _FName_Init FName_Init;

        static FName()
        {
            FName_Init = NativeObject.AcquireFunction<_FName_Init>("FName::Init", 0);
        }

        #endregion

        public static Generated.FName Get(string name, EFindName findName = EFindName.FNAME_Add)
        {
            const int fnamesize = Generated.Implementations.FName_impl.SIZE;
            var obj = new Temp_FName(new byte[fnamesize]);
            FName_Init(ref obj, name, 0, findName, true, -1);
            var nativeMem = Marshal.AllocHGlobal(fnamesize);
            Marshal.Copy(obj.Data, 0, nativeMem, fnamesize);
            return new Generated.Implementations.FName_impl(nativeMem);
        }
    }
}
