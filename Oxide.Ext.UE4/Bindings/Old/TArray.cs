﻿using System;
using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings.Old
{
    [StructLayout(LayoutKind.Sequential)]
    public struct TArray<T> where T : struct
    {
        public readonly int ArrayNum;
        public readonly int ArrayMax;
        public readonly NativePointer Data;

        private TArray(int arrayNum, int arrayMax, NativePointer data)
        {
            ArrayNum = arrayNum;
            ArrayMax = arrayMax;
            Data = data;
        }

        public T this[int idx]
        {
            get
            {
                if (idx < 0 || idx >= ArrayNum) throw new IndexOutOfRangeException();
                return (Data + (Marshal.SizeOf(typeof(T)) * idx)).Deref<T>();
            }
            set
            {
                if (idx < 0 || idx >= ArrayNum) throw new IndexOutOfRangeException();
                (Data + (Marshal.SizeOf(typeof(T)) * idx)).Write(value);
            }
        }

        public T[] ToManagedArray()
        {
            T[] arr = new T[ArrayNum];
            int elementSize = Marshal.SizeOf(typeof(T));
            for (int i = 0; i < ArrayNum; i++)
            {
                arr[i] = (Data + (elementSize * i)).Deref<T>();
            }
            return arr;
        }

        public static Allocated Allocate(int numElements, out TArray<T> arr)
        {
            int elementSize = Marshal.SizeOf(typeof(T));
            NativePointer ptr = Imports.OxideNative_Malloc(elementSize * numElements);
            arr = new TArray<T>(numElements, numElements, ptr);
            return new Allocated(ptr);
        }

        public class Allocated : IDisposable
        {
            public readonly NativePointer Pointer;

            public Allocated(NativePointer ptr)
            {
                Pointer = ptr;
            }

            ~Allocated()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (Pointer != NativePointer.Null)
                    Imports.OxideNative_Free(Pointer);
            }
        }
    }
}
