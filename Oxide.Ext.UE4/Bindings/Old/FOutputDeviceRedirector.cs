﻿namespace Oxide.Ext.UE4.Bindings.Old
{
    public class FOutputDeviceRedirector : NativeObject
    {
        #region Symbols

        public delegate NativePointer _Get();

        private static _Get _get;

        static FOutputDeviceRedirector()
        {
            // Acquire functions
            _get = AcquireFunction<_Get>("FOutputDeviceRedirector::Get");
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the FOutputDeviceRedirector class
        /// </summary>
        /// <param name="nativePtr"></param>
        internal FOutputDeviceRedirector(NativePointer nativePtr)
            : base(nativePtr)
        {


        }

        public static FOutputDevice Get()
        {
            return new FOutputDevice(_get());
        }

    }
}
