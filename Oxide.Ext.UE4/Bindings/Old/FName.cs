﻿using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings.Old
{
    /// <summary>
    /// Represents a UE4 FName object
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FName
    {
        public enum EFindName : int
        {
            FNAME_Find = 0x0,
            FNAME_Add = 0x1,
            FNAME_Replace_Not_Safe_For_Threading = 0x2
        }

        #region Symbols

        [StructLayout(LayoutKind.Sequential)]
        private struct TempString
        {
            public readonly int ArrayNum;
            public readonly int ArrayMax;

            [MarshalAs(UnmanagedType.LPStr)]
            public readonly string Data;

            public TempString(string str)
            {
                Data = str;
                ArrayNum = str.Length + 1;
                ArrayMax = ArrayNum;
            }
        }

        private delegate void _FName_Init(ref FName obj, [MarshalAs(UnmanagedType.LPWStr)] string InName, int InNumber, EFindName FindType, bool bSplitName, int HardcodeIndex);
        private static _FName_Init FName_Init;

        static FName()
        {
            FName_Init = NativeObject.AcquireFunction<_FName_Init>("FName::Init", 0);
        }

        #endregion

        public readonly int ComparisonIndex;
        public readonly int DisplayIndex;
        public readonly uint Number;

        public FName(int comparisonIndex, int displayIndex, uint number)
        {
            ComparisonIndex = comparisonIndex;
            DisplayIndex = displayIndex;
            Number = number;
        }

        public static FName Get(string name, EFindName findName = EFindName.FNAME_Add)
        {
            FName obj = new FName();
            FName_Init(ref obj, name, 0, findName, true, -1);
            return obj;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
