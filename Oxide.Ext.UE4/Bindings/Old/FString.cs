﻿using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings.Old
{
    /// <summary>
    /// Represents a UE4 FString object
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct FString
    {

        #region Symbols

        static FString()
        {

        }

        #endregion

        public readonly TArray<char> Characters;

        private FString(TArray<char> characters)
        {
            Characters = characters;
        }

        /*public FString FromManagedString(string str)
        {

        }*/
    }
}
