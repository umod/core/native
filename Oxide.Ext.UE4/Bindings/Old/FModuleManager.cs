﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Oxide.Ext.UE4.Bindings.Old
{
    // Runtime/Core/Public/Modules/ModuleManager.h: 19
    public enum EModuleLoadResult : int
    {
        /** Module loaded successfully. */
        Success,

        /** The specified module file could not be found. */
        FileNotFound,

        /** The specified module file is incompatible with the module system. */
        FileIncompatible,

        /** The operating system failed to load the module file. */
        CouldNotBeLoadedByOS,

        /** Module initialization failed. */
        FailedToInitialize
    }

    public class FModuleManager : NativeObject
    {
        #region Symbols

        private delegate NativePointer _Get();
        private delegate bool _IsModuleLoaded(NativePointer obj, FName name);

        private static _Get _get;
        private static _IsModuleLoaded _isModuleLoaded;

        static FModuleManager()
        {
            // Acquire functions
            _get = AcquireFunction<_Get>("FModuleManager::Get");
            _isModuleLoaded = AcquireFunction<_IsModuleLoaded>("FModuleManager::IsModuleLoaded");
        }

        #endregion

        [StructLayout(LayoutKind.Sequential)]
        private struct ModuleArrayEntry
        {
            public readonly int Name1, Name2;
            public readonly NativePointer ModuleInfo;
            public readonly NativePointer RefCounter;
            public readonly long Hash;

            public const int Size = 0x20;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct FModuleInfo
        {
            public readonly FString OriginalFilename;
            public readonly FString Filename;
            public readonly NativePointer Handle;
            public readonly SharedPtr Module;
            public readonly bool WasUnloadedAtShutdown;
            public readonly int LoadOrder;

            public const int Size = 0x40;
        }

        private Dictionary<int, NativePointer> modules;

        /// <summary>
        /// Initializes a new instance of the FModuleManager class
        /// </summary>
        /// <param name="nativePtr"></param>
        internal FModuleManager(NativePointer nativePtr)
            : base(nativePtr)
        {

            modules = new Dictionary<int, NativePointer>();
        }

        public static FModuleManager Get()
        {
            NativePointer ptr = _get();
            if (ptr == NativePointer.Null)
                return null;
            else
                return new FModuleManager(ptr);
        }

        public bool IsModuleLoaded(FName name)
        {
            return _isModuleLoaded(nativePtr, name);
        }

        public int ReadModules()
        {
            modules.Clear();
            var logger = Oxide.Core.Interface.Oxide.RootLogger;
            NativePointer listPtr = (nativePtr + 8).Deref<NativePointer>();
            int moduleCount = (nativePtr + 16).Deref<int>();
            for (int i = 0; i < moduleCount; i++)
            {
                NativePointer entryPtr = listPtr + (i * ModuleArrayEntry.Size);
                ModuleArrayEntry arrayEntry = entryPtr.Deref<ModuleArrayEntry>();
                modules.Add(arrayEntry.Name1, arrayEntry.ModuleInfo);
            }
            return moduleCount;
        }

        public T GetModule<T>(FName name) where T : FModule
        {
            // Lookup module info pointer
            NativePointer moduleInfoPtr;
            if (!modules.TryGetValue(name.ComparisonIndex, out moduleInfoPtr)) return null;
            FModuleInfo moduleInfo = moduleInfoPtr.Deref<FModuleInfo>();

            // Get module pointer
            NativePointer modulePtr = moduleInfo.Module.Pointer;

            // Verify it
            if (modulePtr == NativePointer.Null)
                return null;

            // Return
            return Activator.CreateInstance(typeof(T), modulePtr) as T;

        }
    }
}
