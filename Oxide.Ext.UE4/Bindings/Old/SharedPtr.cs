﻿using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings.Old
{
    [StructLayout(LayoutKind.Sequential)]
    public struct SharedPtr
    {
        public readonly NativePointer Pointer;
        public readonly NativePointer RefControllerPointer;
    }
}
