﻿using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4.Bindings.Old
{

    public class FOutputDevice : NativeObject
    {
        #region Symbols

        public delegate void _Log(NativePointer nativePtr, [MarshalAs(UnmanagedType.BStr)] string message);

        private static _Log _log;

        static FOutputDevice()
        {
            // Acquire functions
            _log = AcquireFunction<_Log>("FOutputDevice::Log");
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the FOutputDevice class
        /// </summary>
        /// <param name="nativePtr"></param>
        internal FOutputDevice(NativePointer nativePtr)
            : base(nativePtr)
        {

        }

        /// <summary>
        /// Logs a message to this output device
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            _log(nativePtr, message);
        }

    }
}
