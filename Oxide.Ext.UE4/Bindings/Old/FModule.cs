﻿namespace Oxide.Ext.UE4.Bindings.Old
{
    public class FModule : NativeObject
    {
        /// <summary>
        /// Initializes a new instance of the FModule class
        /// </summary>
        /// <param name="nativePtr"></param>
        public FModule(NativePointer nativePtr)
            : base(nativePtr)
        {
        }
    }
}
