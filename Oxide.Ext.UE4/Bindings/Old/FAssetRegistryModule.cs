﻿namespace Oxide.Ext.UE4.Bindings.Old
{
    public class FAssetRegistryModule : FModule
    {
        #region Symbols

        private delegate NativePointer _Get();

        private static _Get _get;

        static FAssetRegistryModule()
        {
            // Acquire functions
            _get = AcquireFunction<_Get>("FAssetRegistryModule::Get");
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the FAssetRegistryModule class
        /// </summary>
        /// <param name="nativePtr"></param>
        public FAssetRegistryModule(NativePointer nativePtr)
            : base(nativePtr)
        {
        }

        public static NativePointer Get()
        {
            return _get();
        }
    }
}
