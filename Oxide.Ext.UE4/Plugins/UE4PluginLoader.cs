﻿using System;

using Oxide.Core.Plugins;

namespace Oxide.Ext.UE4.Plugins
{
    /// <summary>
    /// Responsible for loading UE4 core plugins
    /// </summary>
    public class UE4PluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(UE4Core) };
    }
}
