﻿using System;

using Oxide.Core;
using Oxide.Core.Logging;
using Oxide.Core.Plugins;
using Oxide.Ext.UE4.Bindings;

namespace Oxide.Ext.UE4.Plugins
{
    /// <summary>
    /// The core UE4 plugin
    /// </summary>
    public class UE4Core : CSPlugin
    {
        private Logger logger;

        /// <summary>
        /// Initializes a new instance of the UE4Core class
        /// </summary>
        public UE4Core()
        {
            // Set attributes
            Name = "ue4core";
            Title = "UE4 Core";
            Author = "Oxide Team";
            Version = new VersionNumber(1, 0, 0);

            // Get logger
            logger = Interface.Oxide.RootLogger;
        }

        /// <summary>
        /// Loads the default config for this plugin
        /// </summary>
        protected override void LoadDefaultConfig()
        {
            // No config yet, we might use it later
            throw new NotImplementedException();
        }

        /// <summary>
        /// Called when
        /// </summary>
        [HookMethod("OnEnginePreInit")]
        private void OnEnginePreInit()
        {
            var assetRegistryName = FName.Get("AssetRegistry", FName.EFindName.FNAME_Find);
            logger.Write(LogType.Debug, "{0}", assetRegistryName.GetComparisonIndex());

            // Look for AssetRegistry
            /*FName assetRegistryName = FName.Get("AssetRegistry", FName.EFindName.FNAME_Find);
            //FAssetRegistryModule assetRegistry = FModuleManager.LoadModuleChecked<FAssetRegistryModule>(assetRegistryName);
            FModuleManager moduleManager = FModuleManager.Get();
            if (!moduleManager.IsModuleLoaded(assetRegistryName))
            {
                logger.Write(LogType.Warning, "AssetRegistry module is not loaded!");
                return;
            }

            NativePointer listPtr = ((NativePointer)moduleManager + 8).Deref<NativePointer>();
            int moduleCount = ((NativePointer)moduleManager + 16).Deref<int>();

            logger.Write(LogType.Debug, "AssetRegistry = {0},{1},{2}", assetRegistryName.Number, assetRegistryName.DisplayIndex, assetRegistryName.ComparisonIndex);
            logger.Write(LogType.Debug, "There are {0} loaded modules.", moduleManager.ReadModules());

            FAssetRegistryModule assetRegistryModule = moduleManager.GetModule<FAssetRegistryModule>(assetRegistryName);
            logger.Write(LogType.Debug, "{0}, {1}", assetRegistryModule, (NativePointer)assetRegistryModule);

            NativePointer vftable = ((NativePointer)assetRegistryModule).Deref<NativePointer>();
            logger.Write(LogType.Debug, "{0}", vftable);
            NativePointer strPtr = Imports.OxideNative_ReverseReflectSymbol(vftable);
            //logger.Write(LogType.Debug, "{0}", strPtr);
            logger.Write(LogType.Debug, " {0}", strPtr.ToNullTerminatedString());*/


            //IntPtr moduleManager_vftable = (IntPtr)Marshal.PtrToStructure(moduleManager.nativePtr, typeof(IntPtr));
            //logger.Write(LogType.Debug, "Module manager is at {0:X}", (long)moduleManager.nativePtr);
            //logger.Write(LogType.Debug, "vftable of the module manager is at {0}", moduleManager_vftable);

            //IntPtr ptr = FAssetRegistryModule.Get();
            //IntPtr vftable = (IntPtr)Marshal.PtrToStructure(ptr, typeof(IntPtr));

            //var assetRegModule = FModuleManager.LoadModuleChecked<FAssetRegistryModule>(assetRegistryName);
            //var sharedPtr = moduleManager.GetModule(assetRegistryName);
            //SharedPtr sharedPtr;
            //var result = moduleManager.LoadModuleWithFailureReason(assetRegistryName, out sharedPtr);
            //logger.Write(LogType.Debug, "{0} {1}", result, sharedPtr.Pointer);


            //logger.Write(LogType.Debug, "0x{0}, 0x{1}", (NativePointer)moduleManager, listPtr);

            //var module = moduleManager.GetModule<FAssetRegistryModule>(assetRegistryName);
            //logger.Write(LogType.Debug, "module = {0}", module);

            //FModule assetRegistry;
            //            var result = moduleManager.LoadModuleWithFailureReason(assetRegistryName, out assetRegistry);

            //logger.Write(LogType.Debug, "{0}, {1}", result, assetRegistry);
            logger.Write(LogType.Debug, "OnEnginePreInit done");
        }

        [HookMethod("OnEnginePostInit")]
        private void OnEnginePostInit()
        {
            // Look for AssetRegistry
            /*FName assetRegistryName = FName.Get("AssetRegistry", FName.EFindName.FNAME_Find);
            //FAssetRegistryModule assetRegistry = FModuleManager.LoadModuleChecked<FAssetRegistryModule>(assetRegistryName);
            FModuleManager moduleManager = FModuleManager.Get();
            if (!moduleManager.IsModuleLoaded(assetRegistryName))
            {
                logger.Write(LogType.Warning, "AssetRegistry module is not loaded!");
                return;
            }
            */
            //var module = FModuleManager.LoadModuleChecked<FAssetRegistryModule>(assetRegistryName);
            //logger.Write(LogType.Debug, "module = {0}", module);

            logger.Write(LogType.Debug, "OnEnginePostInit done");
        }
    }
}
