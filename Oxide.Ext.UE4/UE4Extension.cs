﻿using Oxide.Core;
using Oxide.Core.Extensions;
using Oxide.Ext.UE4.Plugins;

namespace Oxide.Ext.UE4
{
    /// <summary>
    /// The extension class that represents this extension
    /// </summary>
    public class UE4Extension : Extension
    {
        /// <summary>
        /// Gets the name of this extension
        /// </summary>
        public override string Name => "UE4";

        /// <summary>
        /// Gets the version of this extension
        /// </summary>
        public override VersionNumber Version => new VersionNumber(1, 0, 0);

        /// <summary>
        /// Gets the author of this extension
        /// </summary>
        public override string Author => "Oxide Team";

        /// <summary>
        /// Initializes a new instance of the UE4Extension class
        /// </summary>
        /// <param name="manager"></param>
        public UE4Extension(ExtensionManager manager) : base(manager)
        {
        }

        /// <summary>
        /// Loads this extension
        /// </summary>
        public override void Load()
        {
            // Install the logger
            Interface.Oxide.RootLogger.AddLogger(new Logging.NativeLogger());

            // Register the loader
            Manager.RegisterPluginLoader(new UE4PluginLoader());
        }

        /// <summary>
        /// Loads plugin watchers used by this extension
        /// </summary>
        /// <param name="plugindir"></param>
        public override void LoadPluginWatchers(string plugindir)
        {
        }

        /// <summary>
        /// Called when all other extensions have been loaded
        /// </summary>
        public override void OnModLoad()
        {
        }
    }
}
