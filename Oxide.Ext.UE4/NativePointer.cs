﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Oxide.Ext.UE4
{
    /// <summary>
    /// Represents a 64-bit pointer to a native structure or class
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 8)]
    public struct NativePointer : IEquatable<NativePointer>, IEquatable<IntPtr>
    {
        public readonly IntPtr Pointer;

        public NativePointer(IntPtr ptr)
        {
            Pointer = ptr;
        }

        public static readonly NativePointer Null = new NativePointer(IntPtr.Zero);

        public bool Equals(IntPtr other) => Pointer == other;

        public bool Equals(NativePointer other) => Pointer == other.Pointer;

        public bool IsNull { get { return Pointer == IntPtr.Zero; } }

        public string ToNullTerminatedString()
        {
            if (Pointer == IntPtr.Zero) return null;
            var tmp = this;
            char c;
            var sb = new StringBuilder();
            while ((c = tmp.Deref<char>()) != '\0')
            {
                sb.Append(c);
                tmp = tmp + 1;
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return Pointer == IntPtr.Zero;
            if (obj is IntPtr) return Pointer == (IntPtr)obj;
            if (obj is NativePointer) return Pointer == ((NativePointer)obj).Pointer;
            return false;
        }

        public override int GetHashCode() => Pointer.GetHashCode();

        public override string ToString() => Pointer.ToString("X");

        public T Deref<T>() where T : struct => (T)Marshal.PtrToStructure(Pointer, typeof(T));

        public void Write<T>(T value) where T : struct => Marshal.StructureToPtr(value, Pointer, false);

        public static implicit operator NativePointer(IntPtr ptr) => new NativePointer(ptr);

        public static implicit operator IntPtr(NativePointer ptr) => ptr.Pointer;

        public static NativePointer operator +(NativePointer a, long b)
        {
            return new NativePointer((IntPtr)((long)a.Pointer + b));
        }

        public static NativePointer operator +(NativePointer a, NativePointer b)
        {
            return new NativePointer((IntPtr)((long)a.Pointer + (long)b.Pointer));
        }

        public static NativePointer operator -(NativePointer a, long b)
        {
            return new NativePointer((IntPtr)((long)a.Pointer - b));
        }

        public static NativePointer operator -(NativePointer a, NativePointer b)
        {
            return new NativePointer((IntPtr)((long)a.Pointer - (long)b.Pointer));
        }

        public static bool operator ==(NativePointer a, NativePointer b)
        {
            return a.Pointer == b.Pointer;
        }

        public static bool operator !=(NativePointer a, NativePointer b)
        {
            return a.Pointer != b.Pointer;
        }

        public static bool operator <(NativePointer a, NativePointer b)
        {
            return (long)a.Pointer < (long)b.Pointer;
        }

        public static bool operator >(NativePointer a, NativePointer b)
        {
            return (long)a.Pointer > (long)b.Pointer;
        }
    }
}
