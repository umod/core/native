﻿using Oxide.Core.Logging;

namespace Oxide.Ext.UE4.Logging
{
    /// <summary>
    /// A logger that reports to Oxide Native
    /// </summary>
    public class NativeLogger : Logger
    {
        /// <summary>
        /// Initializes a new instance of the NativeLogger class
        /// </summary>
        public NativeLogger() : base(true)
        {
        }

        /// <summary>
        /// Processes the specified log message
        /// </summary>
        /// <param name="message"></param>
        protected override void ProcessMessage(LogMessage message)
        {
            // Call base
            base.ProcessMessage(message);

            // Transform log type and strip the tag
            Imports.OxideNative_LogType logType;
            string msg;
            switch (message.Type)
            {
                case LogType.Debug:
                    logType = Imports.OxideNative_LogType.Debug;
                    msg = message.Message.Replace(" [Debug]", ":");
                    break;
                case LogType.Info:
                    logType = Imports.OxideNative_LogType.Info;
                    msg = message.Message.Replace(" [Info]", ":");
                    break;
                case LogType.Warning:
                    logType = Imports.OxideNative_LogType.Warning;
                    msg = message.Message.Replace(" [Warning]", ":");
                    break;
                case LogType.Error:
                    logType = Imports.OxideNative_LogType.Error;
                    msg = message.Message.Replace(" [Error]", ":");
                    break;
                default:
                    return;
            }

            // Forward it to native
            Imports.OxideNative_WriteLog(logType, msg);
        }
    }
}
