﻿using System;
using System.Runtime.InteropServices;

namespace Oxide.Ext.UE4
{
    /// <summary>
    /// Contains imports from the injected DLL
    /// </summary>
    public static class Imports
    {
        public enum OxideNative_LogType
        {
            Debug,
            Info,
            Warning,
            Error
        }

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OxideNative_WriteLog(
            OxideNative_LogType logType,
            [MarshalAs(UnmanagedType.LPStr)] string message
            );

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr OxideNative_ReflectSymbol(
            [MarshalAs(UnmanagedType.LPStr)] string symbolName,
            int index
            );

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        //[return : MarshalAs(UnmanagedType.LPStr)]
        public static extern IntPtr OxideNative_ReverseReflectSymbol(IntPtr symbolPtr);

        //EXPORT(void*) OxideNative_InstallHook(void *foreign, void *detour)

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr OxideNative_InstallHook(IntPtr foreign, IntPtr detour);

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr OxideNative_Malloc(int sz);

        [DllImport("Oxide.Native.ARK.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void OxideNative_Free(IntPtr ptr);
    }
}
