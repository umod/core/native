/****************\
|* Oxide Native *|
\****************/

// Exception.cpp
// Holds the implementation for OxideNative::Exception

#include "Exception.hpp"

using namespace OxideNative;

Exception::Exception(Exception &existing)
	: message(existing.message)
{

}

Exception::Exception(std::string &message)
	: message(message)
{

}

Exception::Exception(const char *message)
	: message(message)
{

}

/**
 * Gets the exception message
 */
std::string Exception::GetMessage() const
{
	return message;
}