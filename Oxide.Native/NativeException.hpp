/****************\
|* Oxide Native *|
\****************/

// NativeException.hpp
// Holds the definition for OxideNative::NativeException

#pragma once

#include "Exception.hpp"

namespace OxideNative
{
	/**
	 * An exception thrown C++ side
	 */
	class NativeException : public Exception
	{
	private:

		// The error code
		int code;

	public:

		NativeException(NativeException &existing);
		NativeException(std::string &message, int errorCode = 0);
		NativeException(const char *message, int errorCode = 0);

		/**
		 * Gets the exception error code
		 */
		int GetErrorCode() const;


	};


}