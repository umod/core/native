/****************\
|* Oxide Native *|
\****************/

// OxideContext.hpp
// Holds the definition for the OxideContext class

#pragma once

#include "OxideParam.hpp"

#include <string>

typedef void(*Oxide_DebugCallback)(const char *message);

struct IUnknown;

namespace OxideNative
{
	/**
	 * Error code enum that returns from unmanaged export functions
	 */
	namespace ErrorCode
	{
		enum ErrorCode_t
		{
			Unknown = 0x00,
			OK = 0x01,
			ManagedException = 0x02
		};
	}

	/**
	 * Different types of log messages
	 */
	namespace LogType
	{
		enum LogType_t
		{
			Info = 0,
			Debug = 1,
			Warning = 2,
			Error = 3
		};
	}

	/**
	 * Represents the Oxide context
	 */
	class OxideContext
	{
	private:

		IUnknown *context;

		Oxide_DebugCallback debugCallback;

	public:

		OxideContext();
		~OxideContext();

		/**
		 * Sets the debug callback
		 */
		void SetDebugCallback(Oxide_DebugCallback callback);

		/**
		 * Initializes the context
		 */
		void Initialize();

		/**
		 * Writes a log message
		 */
		void WriteLog(LogType::LogType_t logType, const char *message);

		/**
		 * Writes a log message
		 */
		void WriteLog(LogType::LogType_t logType, std::string &message);

		/**
		 * Calls a hook
		 */
		OxideParam CallHook(const char *hookName, int argc, OxideParam *argv);

		/**
		 * Calls a hook
		 */
		OxideParam CallHook(std::string &hookName, int argc, OxideParam *argv);

	private:

		/**
		 * Checks the specified error code for an error
		 */
		bool CheckError(ErrorCode::ErrorCode_t errorCode);

	};
}