/****************\
|* Oxide Native *|
\****************/

// OxideContext.cpp
// Holds the implementation for the OxideContext class

#include "OxideContext.hpp"

#include "NativeException.hpp"
#include "Oxide.Bridge.hpp"

#include <Windows.h>

using namespace OxideNative;

OxideContext::OxideContext()
{
	context = nullptr;
	this->debugCallback = nullptr;
}

OxideContext::~OxideContext()
{
	// Clean up
	if (context != nullptr)
	{
		context->Release();
		context = nullptr;
	}
}

/**
 * Sets the debug callback
 */
void OxideContext::SetDebugCallback(Oxide_DebugCallback callback)
{
	debugCallback = callback;
}

/**
 * Initializes the context
 */
void OxideContext::Initialize()
{
	// Create the context
	Oxide_CreateContext(&context);
	if (context == nullptr)
	{
		throw NativeException("Failed to create Oxide context");
	}

	// Declare error code
	ErrorCode::ErrorCode_t errorCode;

	// Set callback
	if (debugCallback != nullptr)
	{
		errorCode = (ErrorCode::ErrorCode_t)Oxide_SetDebugCallback(context, debugCallback);
		if (!CheckError(errorCode)) return;
	}

	// Initialize the mod
	errorCode = (ErrorCode::ErrorCode_t)Oxide_Initialize(context);
	if (!CheckError(errorCode)) return;
}

/**
 * Writes a log message
 */
void OxideContext::WriteLog(LogType::LogType_t logType, const char *message)
{
	// Declare error code
	ErrorCode::ErrorCode_t errorCode;

	// Make the call
	errorCode = (ErrorCode::ErrorCode_t)Oxide_WriteLog(context, logType, message);
	if (!CheckError(errorCode)) return;
}

/**
 * Writes a log message
 */
void OxideContext::WriteLog(LogType::LogType_t logType, std::string &message)
{
	// Declare error code
	ErrorCode::ErrorCode_t errorCode;

	// Make the call
	errorCode = (ErrorCode::ErrorCode_t)Oxide_WriteLog(context, logType, message.c_str());
	if (!CheckError(errorCode)) return;
}

/**
 * Checks the specified error code for an error
 */
bool OxideContext::CheckError(ErrorCode::ErrorCode_t errorCode)
{
	// Was it OK?
	if (errorCode == ErrorCode::OK) return true;

	// Was it an exception?
	if (errorCode == ErrorCode::ManagedException)
	{
		// Read it
		const char *msg = Oxide_GetException(context);
		if (msg == nullptr) msg = "Unknown managed exception";
		throw NativeException(msg); // TODO: Managed exception?
	}

	// Otherwise...
	throw NativeException("Unknown error");
}

namespace VariantUtil
{

	/**
	 * Converts an Oxide variant to a Windows variant
	 */
	void ToVariant(OxideVariant &src, VARIANT &dst)
	{
		dst.vt = (VARTYPE)src.vt;
		memcpy(&dst.intVal, &src.intVal, 8); // This is extremely very seriously bad
	}

	/**
	 * Converts a Windows variant to an Oxide variant
	 */
	void FromVariant(VARIANT &src, OxideVariant &dst)
	{
		dst.vt = (VariantType::VariantType_t)src.vt;
		memcpy(&dst.intVal, &src.intVal, 8); // This is extremely very seriously bad
	}
}

/**
 * Calls a hook
 */
OxideParam OxideContext::CallHook(const char *hookName, int argc, OxideParam *argv)
{
	// Declare error code
	ErrorCode::ErrorCode_t errorCode;

	// Do we have args?
	if (argc > 0)
	{
		// Start the hook call
		errorCode = (ErrorCode::ErrorCode_t)Oxide_StartCallHook(context, argc);
		if (!CheckError(errorCode)) return OxideParam::Null();

		// Loop each arg
		for (int i = 0; i < argc; i++)
		{
			OxideParam param = argv[i];
			VARIANT toSet;

			switch (param.GetType())
			{
				case ParamType::Null:
					// Technically we don't really need to deal with this arg, but just to be safe
					toSet.vt = VT_EMPTY;
					break;
				case ParamType::Reference:
					// Copy ref in
					// NOTE: Should we be using something that isn't variant here?
					toSet.vt = VT_BYREF;
					toSet.byref = param.GetData();
					break;
				case ParamType::Variant:
					// Copy variant in
					OxideVariant ov;
					memcpy(&ov, param.GetData(), sizeof(OxideVariant));
					VariantUtil::ToVariant(ov, toSet);
					break;
			}

			errorCode = (ErrorCode::ErrorCode_t)Oxide_SetCallHookArg(context, i, toSet);
			if (!CheckError(errorCode)) return OxideParam::Null();
		}
	}

	// Make the call
	VARIANT returnValue;
	errorCode = (ErrorCode::ErrorCode_t)Oxide_CallHook(context, hookName, &returnValue);
	if (!CheckError(errorCode)) return OxideParam::Null();

	// Return it
	if (returnValue.vt == VT_NULL)
		return OxideParam::Null();
	else
	{
		OxideVariant ov;
		VariantUtil::FromVariant(returnValue, ov);
		return OxideParam::Variant(ov);
	}
}

/**
 * Calls a hook
 */
OxideParam OxideContext::CallHook(std::string &hookName, int argc, OxideParam *argv)
{
	return CallHook(hookName.c_str(), argc, argv);
}