/****************\
|* Oxide Native *|
\****************/

// SymbolReflector.hpp
// Holds the definition for the SymbolReflector class

#pragma once

#include <string>

#include "Types.hpp"

namespace OxideNative
{
	namespace PDB
	{
		/**
		 * Represents data about a specified symbol
		 */
		struct SymbolInfo
		{
			ulonglong RVA;

			bool operator==(SymbolInfo &right);
			bool operator!=(SymbolInfo &right);
		};

		typedef void(*SymbolIterator)(const char *name, SymbolInfo &symInfo, void *userData);

		/**
		 * The symbol reflector class
		 */
		class SymbolReflector
		{
		private:

			// The image base
			ulonglong imageBase = 0x140000000ul;

			handle hProcess;
			ulonglong BaseOfDll;

		public:

			/**
			 * Loads the reflector
			 */
			void Load(std::string &exePath);

			/**
			 * Loads the reflector
			 */
			void Load(const char *exePath);

			/**
			 * Gets the specified symbol by name
			 */
			bool Reflect(std::string &name, SymbolInfo *outSymbol, int index = 0);

			/**
			 * Gets the specified symbol by name
			 */
			bool Reflect(const char *name, SymbolInfo *outSymbol, int index = 0);

			/**
			 * Enumerates all loaded symbols
			 */
			void Enumerate(SymbolIterator iterator, void *userData = nullptr);

			/**
			 * Unloads the reflector
			 */
			void Unload();

		};
	}
}