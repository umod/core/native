/****************\
|* Oxide Native *|
\****************/

// Exception.hpp
// Holds the definition for OxideNative::Exception

#pragma once

#include <string>

#undef GetMessage // This is why #define is evil

namespace OxideNative
{
	/**
	 * Represents an exception
	 */
	class Exception
	{
	private:

		// The error message
		std::string message;

	public:

		Exception(Exception &existing);
		Exception(std::string &message);
		Exception(const char *message);

		/**
		 * Gets the exception message
		 */
		std::string GetMessage() const;


	};


}