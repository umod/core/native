/****************\
|* Oxide Native *|
\****************/

// OxideVariant.hpp
// Holds the definition for the OxideVariant class

#pragma once

struct IUnknown;
struct IDispatch;

namespace OxideNative
{
	namespace VariantType
	{
		/**
		 * Represents the type stored in a variant
		 */
		enum VariantType_t
		{
			VT_EMPTY = 0,
			VT_NULL = 1,
			VT_I2 = 2,
			VT_I4 = 3,
			VT_R4 = 4,
			VT_R8 = 5,
			VT_CY = 6,
			VT_DATE = 7,
			VT_BSTR = 8,
			VT_DISPATCH = 9,
			VT_ERROR = 10,
			VT_BOOL = 11,
			VT_VARIANT = 12,
			VT_UNKNOWN = 13,
			VT_DECIMAL = 14,
			VT_I1 = 16,
			VT_UI1 = 17,
			VT_UI2 = 18,
			VT_UI4 = 19,
			VT_INT = 22,
			VT_UINT = 23,
			VT_ARRAY = 0x2000,
			VT_BYREF = 0x4000,
		};
	}

	/**
	 * Represents a variant type
	 */
	struct OxideVariant
	{
		VariantType::VariantType_t vt;

		union
		{
			long long llVal;
			long lVal;
			unsigned char bVal;
			short iVal;
			float fltVal;
			double dblVal;
			short boolVal;
			long scode;
			double date;
			wchar_t *bstrVal;
			IUnknown *punkVal;
			IDispatch *pdispVal;
			unsigned char *pbVal;
			short *piVal;
			long *plVal;
			long long *pllVal;
			float *pfltVal;
			double *pdblVal;
			short *pboolVal;
			long *pscode;
			double *pdate;
			wchar_t **pbstrVal;
			IUnknown **ppunkVal;
			IDispatch **ppdispVal;
			OxideVariant *pvarVal;
			void *byref;
			char cVal;
			unsigned short uiVal;
			unsigned long ulVal;
			unsigned long long ullVal;
			int intVal;
			unsigned int uintVal;
			char *pcVal;
			unsigned short *puiVal;
			unsigned long *pulVal;
			unsigned long long *pullVal;
			int *pintVal;
			unsigned int *puintVal;
		};
	};
}