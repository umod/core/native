/****************\
|* Oxide Native *|
\****************/

// Oxide.Bridge.hpp
// Holds the definition for Oxide.Bridge unmanaged exports

#pragma once

#include <Windows.h>

#define OXIDEAPI(result) extern "C" __declspec(dllimport) result __cdecl

typedef void(*Oxide_DebugCallback)(const char *message);

OXIDEAPI(void) Oxide_CreateContext(IUnknown **context);
OXIDEAPI(int) Oxide_SetDebugCallback(IUnknown *context, Oxide_DebugCallback callback);
OXIDEAPI(int) Oxide_Initialize(IUnknown *context);
OXIDEAPI(int) Oxide_StartCallHook(IUnknown *context, int argCount);
OXIDEAPI(int) Oxide_SetCallHookArg(IUnknown *context, int index, VARIANT value);
OXIDEAPI(int) Oxide_CallHook(IUnknown *context, const char *hookName, VARIANT *returnValue);
OXIDEAPI(int) Oxide_WriteLog(IUnknown *context, int logType, const char *message);
OXIDEAPI(const char *) Oxide_GetException(IUnknown *context);
