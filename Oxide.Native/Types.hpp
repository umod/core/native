/****************\
|* Oxide Native *|
\****************/

// Types.hpp
// Holds common types

#pragma once

namespace OxideNative
{
	typedef unsigned char byte;
	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long ulong;
	typedef unsigned long long ulonglong;
	typedef void *handle;

}