/****************\
|* Oxide Native *|
\****************/

// NativeException.cpp
// Holds the implementation for OxideNative::NativeException

#include "NativeException.hpp"

using namespace OxideNative;

NativeException::NativeException(NativeException &existing)
	: NativeException(existing.GetMessage(), existing.code)
{

}

NativeException::NativeException(std::string &message, int errorCode)
	: Exception(message),
	code(errorCode)
{

}

NativeException::NativeException(const char *message, int errorCode)
	: Exception(message),
	code(errorCode)
{

}

/**
 * Gets the exception error code
 */
int NativeException::GetErrorCode() const
{
	return code;
}