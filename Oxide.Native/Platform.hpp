/****************\
|* Oxide Native *|
\****************/

// Platform.hpp
// Holds the definition for platform specific functionality

#pragma once

#include <string>
#include <vector>

#include "Types.hpp"

#ifdef DeleteFile
#undef DeleteFile
#endif

namespace OxideNative
{
	namespace Platform
	{
		/**
		 * Populates the output vector with the names of all files in a directory
		 */
		void GetFilesInDirectory(std::string &dir, std::vector<std::string> &output);

		/**
		 * Gets if the specified file exists
		 */
		bool FileExists(std::string &fileName);

		/**
		 * Gets if the specified file exists
		 */
		bool FileExists(const char *fileName);

		/**
		 * Gets the full path from the specified relative path
		 */
		std::string GetFullPath(std::string &relPath);

		/**
		 * Gets the full path from the specified relative path
		 */
		std::string GetFullPath(const char *relPath);

		/**
		 * Gets the size of the specified file
		 */
		uint GetFileSize(std::string &path);

		/**
		 * Gets the size of the specified file
		 */
		uint GetFileSize(const char *path);

		/**
		 * Deletes the specified file
		 */
		bool DeleteFile(std::string &path);

		/**
		 * Deletes the specified file
		 */
		bool DeleteFile(const char *path);

	}
}