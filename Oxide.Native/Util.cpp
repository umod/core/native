/****************\
|* Oxide Native *|
\****************/

// Util.cpp
// Holds the implementation for utility functions

#include "Util.hpp"

#include <clocale>
#include <locale>
#include <codecvt>

/**
 * Converts the specified string to a wstring
 */
std::wstring OxideNative::Util::ToWString(std::string &str)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.from_bytes(str);
}

/**
 * Converts the specified wstring to a string
 */
std::string OxideNative::Util::ToString(std::wstring &str)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes(str);
}