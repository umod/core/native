/****************\
|* Oxide Native *|
\****************/

// StringTable.inl
// Holds the definition for the StringMap class

#pragma once

#include <string>
#include <ios>

#include "Types.hpp"

// #define DEBUG_MODE
// #define DEBUG_MODE_TESTS

#ifdef DEBUG_MODE

#include <iostream>
#define DEBUG_LOG(msg) std::cout << "StringMap: " << msg << std::endl

#else

#define DEBUG_LOG(msg) {}

#endif
#ifdef DEBUG_MODE_TESTS

#define TEST_VALUE(val) if (val < 0 || val >= valueCount) throw "TEST_VALUE failed"
#define TEST_TABLE(tbl) if (tbl < 0 || tbl >= tableCount) throw "TEST_TABLE failed"
#define TEST_STRING_OFFSET(off) if (off < 0 || off >= charCount) throw "TEST_STRING_OFFSET failed"

#else

#define TEST_VALUE(val) {}
#define TEST_TABLE(tbl) {}
#define TEST_STRING_OFFSET(tbl) {}

#endif

//#define HASH(c) ((31 ^ c) & 0x07)
#define HASH(c) (c & 0x07)
//#define HASH(c) (((c >> 4) ^ c) & 0x03)
//#define HASH(c) (0)

namespace OxideNative
{
	namespace Structures
	{
		struct StringMapMetrics
		{
			int TableCount, TableMemUsage;
			int ValueCount, ValueMemUsage;
			int CharMemUsage;
			int TotalValueChains;
			int HighestTableRootChain, TotalTableRootChainHeads;
			int OrphanedValues;
			int DuplicateCount;
		};

		typedef unsigned int uint;

		template <class T>
		class StringMap
		{
		private:

			struct Table
			{
				int Entries[8];
				int RootValue;
			};

			struct Value
			{
				int StringOffset;
				int UntrackedStringOffset;
				int NextValue;
				T Item;
			};

			Table *rootTable;
			int tableCount;
			int nextFreeTable;

			Value *rootValue;
			int valueCount;
			int nextFreeValue;

			char *rootChar;
			int charCount;
			int nextFreeChar;

			int duplicateCount;

			const int INITIAL_TABLE_COUNT = 4;
			const int INITIAL_VALUE_COUNT = 16;
			const int INITIAL_CHAR_COUNT = 1024;

		public:

			StringMap()
				: rootTable(nullptr),
				rootValue(nullptr),
				rootChar(nullptr)
			{
				tableCount = valueCount = charCount = 0;
				nextFreeTable = nextFreeValue = nextFreeChar = 0;
				ReallocateTables(INITIAL_TABLE_COUNT);
				ReallocateValues(INITIAL_VALUE_COUNT);
				ReallocateChars(INITIAL_CHAR_COUNT);
				ObtainTable();

				duplicateCount = 0;
			}

			~StringMap()
			{
				if (rootTable != nullptr) delete[] rootTable;
				if (rootValue != nullptr) delete[] rootValue;
				if (rootChar != nullptr) delete[] rootChar;
			}

			void Insert(const char *key, T item)
			{
				//ValidateValues();
				DEBUG_LOG("Inserting " << key);

				// Find and cache length
				int len = strlen(key);
				if (len == 0) throw "Empty string??";

				// Allocate a new value
				int newValueID = ObtainValue();
				TEST_VALUE(newValueID);
				Value *newValue = &rootValue[newValueID];
				newValue->Item = item;

				// Allocate space for the key
				int newStrID = ObtainString(len + 1);
				memcpy(rootChar + newStrID, key, len + 1);
				TEST_STRING_OFFSET(newStrID);
				newValue->StringOffset = newStrID;
				newValue->UntrackedStringOffset = 0;

				// Find the closest table to it
				int table, value;
				char hash;
				int iOut;
				bool b = FindValueIndex(key, len, &table, &value, &hash, &iOut);
				TEST_TABLE(table);
				if (b)
				{
					TEST_VALUE(value);
					
					// It might already exist, let's crawl the value linked list and see...
					Value *toAppendTo = &rootValue[value];
					TEST_STRING_OFFSET(toAppendTo->StringOffset);
					DEBUG_LOG("Value (" << (rootChar + toAppendTo->StringOffset) << ") found at endpoint " << table << " @ " << (int)hash << ", crawling the value's linked list");
					bool found = false;
					do
					{
						found = strcmp(rootChar + toAppendTo->StringOffset, key) == 0;
						if (found) break;
					} while (toAppendTo->NextValue >= 0 && (toAppendTo = &rootValue[toAppendTo->NextValue]) != nullptr);

					// Yup we found it, append to the linked list
					if (found)
					{
						toAppendTo->NextValue = newValueID;
						newValue->UntrackedStringOffset = iOut + 1;
						DEBUG_LOG("Equality passed, appending to the linked list");
						duplicateCount++;

						// Done
						//if (rootValue->Item != 0x1000) throw "Shit got fucked up";
						return;
					}
				}

				// It doesn't exist, where are we inserting it?
				newValue->UntrackedStringOffset = iOut + 1;
				if (hash == -1)
				{
					DEBUG_LOG("Hash was 0xff, search ended in table " << table);

					TEST_TABLE(table);
					Table *tbl = &rootTable[table];

					// Into the root value
					if (tbl->RootValue == -1)
					{
						// Easy peasy
						tbl->RootValue = newValueID;
						DEBUG_LOG("Value set as root value of table");
					}
					else
					{
						// Append it
						Value *toAppendTo = &rootValue[tbl->RootValue];
						while (toAppendTo->NextValue >= 0)
							toAppendTo = &rootValue[toAppendTo->NextValue];
						toAppendTo->NextValue = newValueID;

						DEBUG_LOG("Value appended to linked list of root value of table");
					}
				}
				else
				{
					newValue->UntrackedStringOffset = iOut + 1;

					// What does the slot hold?
					int entry;
					while ((hash != -1) && ((entry = rootTable[table].Entries[hash]) < 0))
					{
						DEBUG_LOG("Another ref occupies the slot (" << entry << ")");
						TEST_TABLE(table);

						// Holds a value. We need to create a new table and move both it and us to there
						// Get existing value
						int oldValueID = -(1 + entry);
						TEST_VALUE(oldValueID);
						Value *oldValue = &rootValue[oldValueID];

						// Allocate a new table, and rewire the old one to point to it
						int newTable = ObtainTable(); // Note: This could invalidate tbl, so we're going to reacquire it
						TEST_TABLE(newTable);
						rootTable[table].Entries[hash] = newTable;
						DEBUG_LOG("We allocated a new table (" << newTable << ") and rewired the previous table (" << table << " @ " << (int)hash << ") to point at it");
						table = newTable;
						Table *tbl = &rootTable[table];

						// Insert old value in
						TEST_STRING_OFFSET(oldValue->StringOffset);
						char oldLastC = rootChar[oldValue->StringOffset + oldValue->UntrackedStringOffset++];
						if (oldLastC == '\0')
						{
							// Goes into root
							tbl->RootValue = oldValueID;
							DEBUG_LOG("Ran out of characters on the old value's string, putting old value in the root slot");
						}
						else
						{
							char oldHash = HASH(oldLastC);
							tbl->Entries[oldHash] = entry;
							DEBUG_LOG("Putting old value in slot " << (int)oldHash);
						}

						// Recalc hash
						char newLastC = key[newValue->UntrackedStringOffset++];
						if (newLastC == '\0')
						{
							DEBUG_LOG("Ran out of characters on the new value's string, putting new value in the root slot");

							// Into the root value
							if (tbl->RootValue == -1)
							{
								DEBUG_LOG("It fits right into the slot");

								// Easy peasy
								tbl->RootValue = newValueID;
							}
							else
							{
								// Append it
								Value *toAppendTo = &rootValue[tbl->RootValue];
								DEBUG_LOG("Something already occupies the slot (" << (rootChar + toAppendTo->StringOffset) << "), appending to the value's linked list");
								while (toAppendTo->NextValue >= 0)
									toAppendTo = &rootValue[toAppendTo->NextValue];
								toAppendTo->NextValue = newValueID;
							}	

							// Make sure we're done
							hash = -1;
						}
						else
						{
							DEBUG_LOG("We're at '" << newLastC << "' now, recalculating hash and heading for next iteration");
							hash = HASH(newLastC);
						}
					}
					if (entry > 0)
					{
						// Holds a table. We shouldn't really have got here
						throw "How did this happen?";
					}
					else if (hash != -1)
					{
						// It's empty, transform and set
						DEBUG_LOG("We're nearly done, inserting value " << newValueID << " at table " << table << " @ " << (int)hash);
						TEST_TABLE(table);
						rootTable[table].Entries[hash] = -(1 + newValueID);
					}
				}
				//if (rootValue->Item != 0x1000) throw "Shit got fucked up";
			}

			bool Find(const char *key, int duplicateIndex, T *outItem) const
			{
				int table, value;
				if (!FindValueIndex(key, strlen(key), &table, &value)) return false;
				Value *toScan = &rootValue[value];
				bool found = false;
				do
				{
					found = strcmp(rootChar + toScan->StringOffset, key) == 0;
					if (found && (duplicateIndex--) == 0) break;
				} while (toScan->NextValue >= 0 && (toScan = &rootValue[toScan->NextValue]) != nullptr);
				*outItem = toScan->Item;
				return found && duplicateIndex < 0;
			}

			bool SlowFind(const char *key, int duplicateIndex, T *outItem) const
			{
				for (int i = 0; i < nextFreeValue; i++)
				{
					Value *val = &rootValue[i];
					if (strcmp(rootChar + val->StringOffset, key) == 0)
					{
						duplicateIndex--;
						if (duplicateIndex == -1)
						{
							*outItem = val->Item;
							return true;
						}
					}
				}
				return false;
			}

			int FindValueChainSize(Value *val) const
			{
				int cnt = 0;
				int nextValue = val->NextValue;
				while (nextValue != -1)
				{
					nextValue = rootValue[nextValue].NextValue;
					cnt++;
				}
				return cnt;
			}

			StringMapMetrics GetMetrics() const
			{
				StringMapMetrics metrics;
				metrics.TableCount = nextFreeTable;
				metrics.TableMemUsage = sizeof(Table) * nextFreeTable;
				metrics.ValueCount = nextFreeValue;
				metrics.ValueMemUsage = sizeof(Value) * nextFreeValue;
				metrics.CharMemUsage = nextFreeChar;
				metrics.TotalValueChains = 0;
				metrics.TotalTableRootChainHeads = 0;
				metrics.HighestTableRootChain = 0;
				metrics.DuplicateCount = duplicateCount;
				for (int i = 0; i < nextFreeValue; i++)
				{
					if (rootValue[i].StringOffset < 0)
						throw "Uh oh";
					if (rootValue[i].NextValue != -1)
						metrics.TotalValueChains++;
				}
				for (int i = 0; i < nextFreeTable; i++)
				{
					if (rootTable[i].RootValue != -1)
					{
						metrics.TotalTableRootChainHeads++;
						int chainSize = FindValueChainSize(&rootValue[rootTable[i].RootValue]);
						if (chainSize > metrics.HighestTableRootChain)
							metrics.HighestTableRootChain = chainSize;
					}
				}
				return metrics;
			}

			const char *ReverseLookup(T item) const
			{
				for (int i = 0; i < nextFreeValue; i++)
				{
					Value *val = &rootValue[i];
					if (val->Item == item)
						return (const char*)&rootChar[val->StringOffset];
				}
				return nullptr;
			}

			struct SerialisedHeader
			{
				char magic[4];
				int tableCount, valueCount, charCount;
				ulonglong signature;
			};

			void Serialise(ulonglong signature, std::ostream &strm) const
			{
				SerialisedHeader header;
				header.magic[0] = 'S'; header.magic[1] = 'T'; header.magic[2] = 'R'; header.magic[3] = 'M';
				header.tableCount = nextFreeTable;
				header.valueCount = nextFreeValue;
				header.charCount = nextFreeChar;
				header.signature = signature;
				strm.write(reinterpret_cast<char*>(&header), sizeof(header));
				strm.write(reinterpret_cast<char*>(rootTable), sizeof(Table) * nextFreeTable);
				strm.write(reinterpret_cast<char*>(rootValue), sizeof(Value) * nextFreeValue);
				strm.write(rootChar, sizeof(char) * nextFreeChar);
			}

			bool Deserialise(std::istream &strm, ulonglong expectedSignature)
			{
				SerialisedHeader header;
				strm.read(reinterpret_cast<char*>(&header), sizeof(header));
				if (header.magic[0] != 'S' || header.magic[1] != 'T' || header.magic[2] != 'R' || header.magic[3] != 'M')
					return false;
				if (header.signature != expectedSignature)
					return false;

				if (rootTable != nullptr) delete[] rootTable;
				if (rootValue != nullptr) delete[] rootValue;
				if (rootChar != nullptr) delete[] rootChar;

				nextFreeTable = tableCount = header.tableCount;
				rootTable = new Table[tableCount];
				strm.read(reinterpret_cast<char*>(rootTable), sizeof(Table) * tableCount);

				nextFreeValue = valueCount = header.valueCount;
				rootValue = new Value[valueCount];
				strm.read(reinterpret_cast<char*>(rootValue), sizeof(Value) * valueCount);

				nextFreeChar = charCount = header.charCount;
				rootChar = new char[charCount];
				strm.read(rootChar, sizeof(char) * charCount);

				return true;
			}

		private:

			void ValidateValues()
			{
				for (int i = 0; i < nextFreeValue; i++)
				{
					if (rootValue[i].StringOffset < 0)
						throw "Uh oh";
				}
			}

			void ReallocateTables(int count)
			{
				Table *tmp = new Table[count];
				if (rootTable != nullptr)
				{
					memcpy(tmp, rootTable, sizeof(Table) * tableCount);
					if (count > tableCount)
						memset(tmp + tableCount, 0, sizeof(Table) * (count - tableCount));
					else
						throw "Allocating down not supported";
					delete[] rootTable;
				}
				rootTable = tmp;
				tableCount = count;
				DEBUG_LOG("ReallocateTables " << count);
			}

			void ReallocateValues(int count)
			{
				Value *tmp = new Value[count];
				if (rootValue != nullptr)
				{
					memcpy(tmp, rootValue, sizeof(Value) * valueCount);
					if (count > valueCount)
						memset(tmp + valueCount, 0, sizeof(Value) * (count - valueCount));
					else
						throw "Allocating down not supported";
					delete[] rootValue;
				}
				rootValue = tmp;
				valueCount = count;
				DEBUG_LOG("ReallocateValues " << count);
			}

			void ReallocateChars(int count)
			{
				char *tmp = new char[count];
				if (rootChar != nullptr)
				{
					memcpy(tmp, rootChar, sizeof(char) * charCount);
					if (count > charCount)
						memset(tmp + charCount, 0, sizeof(char) * (count - charCount));
					else
						throw "Allocating down not supported";
					delete[] rootChar;
				}
				rootChar = tmp;
				charCount = count;
				DEBUG_LOG("ReallocateChars " << count);
			}

			int ObtainTable()
			{
				if (nextFreeTable >= tableCount)
				{
					ReallocateTables(tableCount << 1);
				}
				int ret = nextFreeTable++;
				memset(rootTable + ret, 0, sizeof(Table));
				rootTable[ret].RootValue = -1;
				return ret;
			}

			int ObtainValue()
			{
				if (nextFreeValue >= valueCount)
				{
					ReallocateValues(valueCount << 1);
				}
				int ret = nextFreeValue++;
				rootValue[ret].StringOffset = -1;
				rootValue[ret].NextValue = -1;
				rootValue[ret].UntrackedStringOffset = -1;
				return ret;
			}

			int ObtainString(int sz)
			{
				while (nextFreeChar + sz > charCount)
				{
					ReallocateChars(charCount << 1);
				}
				int ret = nextFreeChar;
				nextFreeChar += sz;
				return ret;
			}

			bool FindValueIndex(const char *str, int len, int *table, int *value, char *hashOut = nullptr, int *strIOut = nullptr) const
			{
				// Start at root
				int curTable = 0;

				// Iterate each char
				for (int i = 0; i < len; i++)
				{
					// Hash it
					char c = str[i];
					char hash = HASH(c);

					// Get the table
					Table *tbl = &rootTable[curTable];
					int entry = tbl->Entries[hash];

					// Is it a value?
					if (entry < 0)
					{
						// We found it
						*table = curTable;
						*value = -(1 + entry);
						if (hashOut != nullptr) *hashOut = hash;
						if (strIOut != nullptr) *strIOut = i;
						return true;
					}
					else if (entry == 0)
					{
						// Not found
						*table = curTable;
						*value = 0;
						if (hashOut != nullptr) *hashOut = hash;
						if (strIOut != nullptr) *strIOut = i;
						return false;
					}

					// Next table
					curTable = entry;
				}

				// Must be the root value
				Table *tbl = &rootTable[curTable];
				*table = curTable;
				if (hashOut != nullptr) *hashOut = -1;
				if (strIOut != nullptr) *strIOut = len;
				if (tbl->RootValue >= 0)
				{
					*value = tbl->RootValue;
					return true;
				}
				else
				{
					*value = 0;
					return false;
				}
			}



		};
	}
}

#undef HASH