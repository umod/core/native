/****************\
|* Oxide Native *|
\****************/

// SymbolReflector.cpp
// Holds the implementation for the SymbolReflector class

#include "SymbolReflector.hpp"
#include "NativeException.hpp"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <dbghelp.h>

using namespace OxideNative::PDB;

bool SymbolInfo::operator==(SymbolInfo &right)
{
	return RVA == right.RVA;
}
bool SymbolInfo::operator!=(SymbolInfo &right)
{
	return RVA != right.RVA;
}

/**
 * Loads all symbols
 */
void SymbolReflector::Load(std::string &exePath)
{
	// Redirect
	Load(exePath.c_str());
}

/**
 * Loads all symbols
 */
void SymbolReflector::Load(const char *exePath)
{
	// Get process handle
	hProcess = GetCurrentProcess();

	// Set options
	SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS);

	// Initialize
	if (!SymInitialize(hProcess, NULL, TRUE))
	{
		// SymInitialize failed
		char tmp[64];
		sprintf_s(tmp, "SymInitialize returned error: %d\n", GetLastError());
		throw OxideNative::NativeException(tmp);
	}

	// Load the module
	DWORD64 dwBaseAddr = 0;
	BaseOfDll = SymLoadModuleEx(hProcess,    // target process 
		NULL,        // handle to image - not used
		exePath, // name of image file
		NULL,        // name of module - not required
		dwBaseAddr,  // base address - not required
		0,           // size of image - not required
		NULL,        // MODLOAD_DATA used for special cases 
		0);
	if (BaseOfDll == 0)
	{
		// SymLoadModuleEx failed
		char tmp[64];
		sprintf_s(tmp, "SymLoadModuleEx returned error: %d\n", GetLastError());
		throw OxideNative::NativeException(tmp);
	}
}

/**
 * Gets the specified symbol by name
 */
bool SymbolReflector::Reflect(std::string &name, SymbolInfo *outSymbol, int index)
{
	return Reflect(name.c_str(), outSymbol, index);
}

/**
 * Gets the specified symbol by name
 */
bool SymbolReflector::Reflect(const char *name, SymbolInfo *outSymbol, int index)
{
	struct PassData
	{
		ulonglong addr;
		bool success;
		int idx;
		int target;
	} passData;

	passData.addr = 0;
	passData.success = false;
	passData.idx = 0;
	passData.target = index;

	PSYM_ENUMERATESYMBOLS_CALLBACK lamba = [](PSYMBOL_INFO info, ulong sz, void* ptr) -> BOOL
	{
		PassData *passData = (PassData*)ptr;
		if (passData->idx++ == passData->target)
		{
			passData->success = true;
			passData->addr = info->Address;
		}
		return true;
	};
	SymEnumSymbols(hProcess, BaseOfDll, name, lamba, &passData);

	if (passData.success)
	{
		outSymbol->RVA = passData.addr - imageBase;
		return true;
	}
	else
		return false;
}

/**
 * Enumerates all loaded symbols
 */
void SymbolReflector::Enumerate(SymbolIterator iterator, void *userData)
{
	struct PassData
	{
		SymbolIterator iterator;
		ulonglong imageBase;
		void *userData;
	} passData;
	passData.iterator = iterator;
	passData.imageBase = imageBase;
	passData.userData = userData;
	PSYM_ENUMERATESYMBOLS_CALLBACK lamba = [](PSYMBOL_INFO info, ulong sz, void* ptr) -> BOOL
	{
		PassData *passData = (PassData*)ptr;
		SymbolInfo symInfo;
		symInfo.RVA = info->Address - passData->imageBase;
		passData->iterator(info->Name, symInfo, passData->userData);
		return true;
	};
	SymEnumSymbols(hProcess, BaseOfDll, "*", lamba, &passData);
}

/**
 * Unloads the reflector
 */
void SymbolReflector::Unload()
{
	SymCleanup(hProcess);
}