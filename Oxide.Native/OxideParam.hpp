/****************\
|* Oxide Native *|
\****************/

// OxideContext.hpp
// Holds the definition for the OxideParam class

#pragma once

#include "OxideVariant.hpp"

namespace OxideNative
{
	/**
	 * Represents a parameter type
	 */
	namespace ParamType
	{
		enum ParamType_t
		{
			Null,
			Reference,
			Variant
		};
	}

	/**
	 * Represents a parameter to a hook call
	 */
	class OxideParam
	{
	private:

		// The type of this parameter
		ParamType::ParamType_t type;

		// Reference data
		void *data;

	public:

		OxideParam();
		OxideParam(ParamType::ParamType_t type, void *data);

		/**
		 * Gets the type of this parameter
		 */
		ParamType::ParamType_t GetType() const;

		/**
		 * Gets the data of this parameter
		 */
		void *GetData() const;

		/**
		 * Makes this parameter inactive
		 */
		void Release();

		/**
		 * Gets a null parameter
		 */
		static OxideParam Null();

		/**
		 * Gets a reference parameter
		 */
		static OxideParam Reference(void *ptr);

		/**
		 * Gets a variant parameter
		 */
		static OxideParam Variant(OxideVariant &variant);

#define DeclareVariantOverload(_type_) static OxideParam Variant(_type_)

		DeclareVariantOverload(wchar_t *);
		DeclareVariantOverload(int);
	};
}