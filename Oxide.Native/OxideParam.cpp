/****************\
|* Oxide Native *|
\****************/

// OxideContext.cpp
// Holds the implementation for the OxideParam class

#include "OxideParam.hpp"

#include <memory>

using namespace OxideNative;

OxideParam::OxideParam()
	: type(ParamType::Null),
	data(nullptr)
{

}

OxideParam::OxideParam(ParamType::ParamType_t type, void *data)
	: type(type),
	data(data)
{
	
}

/**
 * Gets the type of this parameter
 */
ParamType::ParamType_t OxideParam::GetType() const
{
	return this->type;
}

/**
 * Gets the data of this parameter
 */
void *OxideParam::GetData() const
{
	return this->data;
}

/**
 * Makes this parameter inactive
 */
void OxideParam::Release()
{
	if (GetType() == ParamType::Variant && data != nullptr)
	{
		delete data;
		data = nullptr;
		type = ParamType::Null;
	}
}

/**
 * Gets a null parameter
 */
OxideParam OxideParam::Null()
{
	return OxideParam(ParamType::Null, nullptr);
}

/**
 * Gets a reference parameter
 */
OxideParam OxideParam::Reference(void *ptr)
{
	return OxideParam(ParamType::Reference, ptr);
}

/**
 * Gets a variant parameter
 */
OxideParam OxideParam::Variant(OxideVariant &variant)
{
	return OxideParam(ParamType::Variant, new OxideVariant(variant));
}

#define DefineVariantOverload(_type_, _field_, _vt_) OxideParam OxideParam::Variant(_type_ val) { OxideVariant v; memset(&v, 0, sizeof(OxideVariant)); v._field_ = val; v.vt = _vt_; return Variant(v); }

DefineVariantOverload(wchar_t *,	bstrVal,	VariantType::VT_BSTR)
DefineVariantOverload(int,			intVal,		VariantType::VT_INT)