/****************\
|* Oxide Native *|
\****************/

// Windows.cpp
// Holds the implementation for windows specific functionality

#ifdef WIN32

#include "Platform.hpp"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Shlwapi.h>
#include <sys/stat.h>

#ifdef DeleteFile
#undef DeleteFile // This is why #define is evil
#endif

/**
 * Populates the output vector with the names of all files in a directory
 */
void OxideNative::Platform::GetFilesInDirectory(std::string &dir, std::vector<std::string> &output)
{
	char search_path[200];
	sprintf_s(search_path, "%s*.*", dir.c_str());
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path, &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				output.push_back(fd.cFileName);
			}
		}
		while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
}

/**
 * Gets if the specified file exists
 */
bool OxideNative::Platform::FileExists(std::string &fileName)
{
	return PathFileExists(fileName.c_str()) != 0;
}

/**
 * Gets if the specified file exists
 */
bool OxideNative::Platform::FileExists(const char *fileName)
{
	return PathFileExists(fileName) != 0;
}

/**
 * Gets the full path from the specified relative path
 */
std::string OxideNative::Platform::GetFullPath(std::string &relPath)
{
	const int BUFFER_SIZE = 256;
	char tmp[BUFFER_SIZE];
	GetFullPathName(relPath.c_str(), BUFFER_SIZE, tmp, NULL);
	return std::string(tmp);
}

/**
 * Gets the full path from the specified relative path
 */
std::string OxideNative::Platform::GetFullPath(const char *relPath)
{
	const int BUFFER_SIZE = 256;
	char tmp[BUFFER_SIZE];
	GetFullPathName(relPath, BUFFER_SIZE, tmp, NULL);
	return std::string(tmp);
}

/**
 * Gets the size of the specified file
 */
OxideNative::uint OxideNative::Platform::GetFileSize(std::string &path)
{
	return GetFileSize(path.c_str());
}

/**
 * Gets the size of the specified file
 */
OxideNative::uint OxideNative::Platform::GetFileSize(const char *path)
{
	struct stat sb;
	if (stat(path, &sb) == -1) return 0;
	return sb.st_size;
}

/**
 * Deletes the specified file
 */
bool OxideNative::Platform::DeleteFile(std::string &path)
{
	return DeleteFile(path.c_str()) != 0;
}

/**
 * Deletes the specified file
 */
bool OxideNative::Platform::DeleteFile(const char *path)
{
	return ::DeleteFileA(path) != 0;
}

#endif