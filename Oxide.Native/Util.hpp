/****************\
|* Oxide Native *|
\****************/

// Util.hpp
// Holds the definition for utility functions

#pragma once

#include <string>

namespace OxideNative
{
	namespace Util
	{
		/**
		 * Converts the specified string to a wstring
		 */
		std::wstring ToWString(std::string &str);

		/**
		 * Converts the specified wstring to a string
		 */
		std::string ToString(std::wstring &str);
	}
}