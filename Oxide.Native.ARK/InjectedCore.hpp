/****************\
|* Oxide Native *|
\****************/

// InjectedCore.hpp
// Holds the definition for the InjectedCore class

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "../Oxide.Native/NativeException.hpp"
#include "../Oxide.Native/OxideContext.hpp"
#include "../Oxide.Native/StringMap.inl"
#include "../Oxide.Native/SymbolReflector.hpp"

#include "Common.hpp"

namespace OxideNativeArk
{

	/**
	 * The injected core class
	 */
	class InjectedCore
	{
	private:

		ulong myProcessID;
		ulong mainThreadId;
		DWORD_PTR imageBase;

		HANDLE logPipe;
		OVERLAPPED sendO;

		OxideNative::OxideContext oxideContext;

		bool oxideLoaded;

		//OxideNative::PDB::SymbolReflector symReflector;
		OxideNative::Structures::StringMap<OxideNative::PDB::SymbolInfo> symbolMap;


		const bool USE_DEBUG_CALLBACK = false;

	public:

		// Singleton instance
		static InjectedCore *Instance;

		InjectedCore();
		~InjectedCore();

		void Initialize();

		void WriteLog(LogType::LogType_t type, const char *message);
		void WriteLog(LogType::LogType_t type, std::string &message);

		bool LookupFunctionSymbol(const char *name, int index, void **ptr);
		bool ReverseLookupSymbol(void *symbol, const char **name);

		bool TryLoadOxide();

		OxideNative::OxideContext &GetNativeContext();

	private:

		void SetupPipe();
		void SendPacket(PacketType::PacketType_t packetType, void *data, int packetSize);

		ulonglong CalculateSymbolMapSignature();

		void GenerateSymbols();

		static void DebugCallback(const char *message);

		static DWORD GetMainThreadId();

	};
}