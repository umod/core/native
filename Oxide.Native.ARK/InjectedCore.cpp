/****************\
|* Oxide Native *|
\****************/

// InjectedCore.cpp
// Holds the implementation for the InjectedCore class

#include "InjectedCore.hpp"
#include "Detours.hpp"

#include "../Oxide.Native/Platform.hpp"
#include "../Oxide.Native/Util.hpp"

#include <TlHelp32.h>
#include <fstream>

using namespace OxideNativeArk;
using namespace OxideNative::PDB;

#define THROW_NATIVE(message) throw message;

#ifdef DeleteFile
#undef DeleteFile
#endif

// Singleton instance
InjectedCore *InjectedCore::Instance;

InjectedCore::InjectedCore()
	: myProcessID(0),
	imageBase(0),
	logPipe(INVALID_HANDLE_VALUE),
	oxideLoaded(false)
{
	// Prepare sendO
	ZeroMemory(&sendO, sizeof(sendO));
}

InjectedCore::~InjectedCore()
{
	// Close pipe
	if (logPipe != INVALID_HANDLE_VALUE)
	{
		CloseHandle(logPipe);
		logPipe = INVALID_HANDLE_VALUE;
	}
}

void InjectedCore::Initialize()
{
	using namespace OxideNative::Platform;

	// Get process ID
	myProcessID = GetCurrentProcessId();

	// Setup pipe
	SetupPipe();

	// Look for symbol file
	bool shouldGenSymbols = false;
	if (!FileExists(GetFullPath("oxide_symbol_map.dat")))
	{
		shouldGenSymbols = true;
		WriteLog(LogType::Info, "Symbol map file not found, generating new one...");
	}
	else
	{
		std::ifstream file;
		file.open("oxide_symbol_map.dat", std::ios_base::binary);
		if (!file)
		{
			shouldGenSymbols = true;
			WriteLog(LogType::Info, "Symbol map found but could not be opened, generating new one...");
		}
		WriteLog(LogType::Info, "Loading symbols...");
		ulonglong sig = CalculateSymbolMapSignature();
		if (!symbolMap.Deserialise(file, sig))
		{
			shouldGenSymbols = true;
			WriteLog(LogType::Info, "Symbol map found but was corrupt or out-of-date, generating new one...");
			file.close();
			DeleteFile(GetFullPath("oxide_symbol_map.dat"));
		}
		else
		{
			file.close();
		}
	}
	if (shouldGenSymbols) GenerateSymbols();
	{
		char tmp[128];
		OxideNative::Structures::StringMapMetrics metrics = symbolMap.GetMetrics();
		int totalMemoryUsage = metrics.CharMemUsage + metrics.TableMemUsage + metrics.ValueMemUsage;
		sprintf_s(tmp, "Loaded %d symbols. The symbol map is using ~%dMiB of memory.", metrics.ValueCount, totalMemoryUsage >> 20);
		WriteLog(LogType::Info, tmp);
	}

	// Detour things
	imageBase = (DWORD_PTR)GetModuleHandle(NULL);
	WriteLog(LogType::Info, "Installing detours...");
	try
	{
		Detours::Initialize(symbolMap, imageBase);
		Detours::Instance->InstallHooks();
	}
	catch (OxideNative::NativeException ex)
	{
		WriteLog(LogType::Error, ex.GetMessage());
		return;
	}

	// Get main thread
	mainThreadId = GetMainThreadId();

	// Let host process know that things went well
	Packet_InjectComplete packet;
	packet.mainThreadId = mainThreadId;
	SendPacket(PacketType::InjectComplete, &packet, sizeof(packet));
}

void InjectedCore::WriteLog(LogType::LogType_t type, const char *message)
{
	// Assemble packet
	int packetSize = strlen(message) + 2;
	char *packet = new char[packetSize];
	packet[0] = (char)type;
	memcpy(packet + 1, message, packetSize - 1);

	// Send
	SendPacket(PacketType::LogMessage, packet, packetSize);

	// Clean up
	delete[] packet;
}

void InjectedCore::WriteLog(LogType::LogType_t type, std::string &message)
{
	WriteLog(type, message.c_str());
}

bool InjectedCore::LookupFunctionSymbol(const char *name, int index, void **ptr)
{
	SymbolInfo symb;
	if (!symbolMap.Find(name, index, &symb)) return false;
	*ptr = (void*)(imageBase + symb.RVA);
	return true;
}

bool InjectedCore::ReverseLookupSymbol(void *symbol, const char **name)
{
	SymbolInfo symb;
	symb.RVA = (ulonglong)symbol - imageBase;
	*name = symbolMap.ReverseLookup(symb);
	return *name != nullptr;
}

void InjectedCore::SetupPipe()
{
	// Get pipe name
	char buffer[32];
	sprintf_s(buffer, "\\\\.\\pipe\\OxideNative_%i", myProcessID);

	// Create pipe
	while (logPipe == INVALID_HANDLE_VALUE)
	{
		logPipe = CreateFileA(buffer, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
		if (logPipe != INVALID_HANDLE_VALUE) break;

		DWORD err = GetLastError();
		if (err != ERROR_PIPE_BUSY)
		{
			char *ex_buf = new char[32];
			sprintf_s(ex_buf, 32, "Could not open pipe. (%d)", err);
			THROW_NATIVE(ex_buf);
		}

		if (!WaitNamedPipeA(buffer, 1000))
		{
			THROW_NATIVE("Could not open pipe - timed out.");
		}
	}

	// Change to message mode
	DWORD mode = PIPE_READMODE_MESSAGE;
	if (!SetNamedPipeHandleState(logPipe, &mode, NULL, NULL))
	{
		// SetNamedPipeHandleState failed.
		THROW_NATIVE("SetNamedPipeHandleState failed.");
	}

	// Setup event for overlapping
	sendO.hEvent = CreateEvent(NULL, true, true, NULL);
}

void InjectedCore::SendPacket(PacketType::PacketType_t packetType, void *data, int packetSize)
{
	// Assemble packet
	unsigned char *packet = new unsigned char[packetSize + 1];
	packet[0] = (unsigned char)packetType;
	memcpy(packet + 1, data, packetSize);

	// Send
	bool success = true;
	DWORD written = 0;
	success = WriteFile(logPipe, packet, packetSize, &written, &sendO) != 0;

	if (success && written == packetSize)
	{
		// It worked
		delete[] packet;
		return;
	}

	DWORD err = GetLastError();
	while (!success && err == ERROR_IO_PENDING)
	{
		// Wait
		WaitForSingleObject(sendO.hEvent, INFINITE);

		// Check
		success = GetOverlappedResult(logPipe, &sendO, &written, false) != 0;
	}
	delete[] packet;

	if (!success)
	{
		// Error
		//criticalError = "Packet send failed";
		THROW_NATIVE("Packet send failed");
	}

	// Success
	return;
}

ulonglong InjectedCore::CalculateSymbolMapSignature()
{
	using namespace OxideNative;
	uint exeSize = Platform::GetFileSize("ShooterGameServer.exe");
	uint pdbSize = Platform::GetFileSize("ShooterGameServer.pdb");
	return exeSize | (pdbSize << 32ull);
}

void InjectedCore_SymbolIterator(const char *name, SymbolInfo &symbolInfo, void *userData)
{
	using namespace OxideNative::PDB;
	using namespace OxideNative::Structures;
	StringMap<SymbolInfo> *symbolMap = (StringMap<SymbolInfo>*)userData;
	symbolMap->Insert(name, symbolInfo);
}

void InjectedCore::GenerateSymbols()
{
	using namespace OxideNative::Platform;
	using namespace OxideNative::PDB;
	SymbolReflector symRefl;
	symRefl.Load(GetFullPath("ShooterGameServer.exe"));
	symRefl.Enumerate(InjectedCore_SymbolIterator, &symbolMap);
	symRefl.Unload();
	WriteLog(LogType::Info, "Saving symbols to file...");
	std::ofstream file;
	file.open("oxide_symbol_map.dat", std::ios_base::binary);
	if (!file)
	{
		WriteLog(LogType::Warning, "Failed to open file for writing, symbols will not be cached.");
		return;
	}
	symbolMap.Serialise(CalculateSymbolMapSignature(), file);
	file.close();
}

bool InjectedCore::TryLoadOxide()
{
	WriteLog(LogType::Info, "Initialising Oxide...");
	if (USE_DEBUG_CALLBACK) oxideContext.SetDebugCallback(&DebugCallback);
	try
	{
		oxideContext.Initialize();
	}
	catch (OxideNative::NativeException ex)
	{
		WriteLog(LogType::Error, ex.GetMessage());
		return false;
	}

	// Success
	WriteLog(LogType::Info, "Oxide has been initialised.");
	return true;
}

OxideNative::OxideContext &InjectedCore::GetNativeContext()
{
	return oxideContext;
}

void InjectedCore::DebugCallback(const char *message)
{
	Instance->WriteLog(LogType::Info, message);
}

#ifndef MAKEULONGLONG
#define MAKEULONGLONG(ldw, hdw) ((ULONGLONG(hdw) << 32) | ((ldw) & 0xFFFFFFFF))
#endif

#ifndef MAXULONGLONG
#define MAXULONGLONG ((ULONGLONG)~((ULONGLONG)0))
#endif

DWORD InjectedCore::GetMainThreadId()
{
	DWORD dwMainThreadID = 0;
	ULONGLONG ullMinCreateTime = MAXULONGLONG;

	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	if (hThreadSnap != INVALID_HANDLE_VALUE) {
		THREADENTRY32 th32;
		th32.dwSize = sizeof(THREADENTRY32);
		BOOL bOK = TRUE;
		for (bOK = Thread32First(hThreadSnap, &th32); bOK;
		bOK = Thread32Next(hThreadSnap, &th32)) {
			if (th32.th32OwnerProcessID == GetCurrentProcessId()) {
				HANDLE hThread = OpenThread(THREAD_QUERY_INFORMATION,
					TRUE, th32.th32ThreadID);
				if (hThread) {
					FILETIME afTimes[4] = { 0 };
					if (GetThreadTimes(hThread,
						&afTimes[0], &afTimes[1], &afTimes[2], &afTimes[3])) {
						ULONGLONG ullTest = MAKEULONGLONG(afTimes[0].dwLowDateTime,
							afTimes[0].dwHighDateTime);
						if (ullTest && ullTest < ullMinCreateTime) {
							ullMinCreateTime = ullTest;
							dwMainThreadID = th32.th32ThreadID; // Let it be main... :)
						}
					}
					CloseHandle(hThread);
				}
			}
		}
#ifndef UNDER_CE
		CloseHandle(hThreadSnap);
#else
		CloseToolhelp32Snapshot(hThreadSnap);
#endif
	}

	return dwMainThreadID;
}
