/****************\
|* Oxide Native *|
\****************/

// Detours.cpp
// Holds the implementation for the Detours class

#include "Detours.hpp"

#include "../Oxide.Native/NativeException.hpp"

#include "../Dependencies/include/MinHook.h"

#include "InjectedCore.hpp"

#undef GetMessage

using namespace OxideNativeArk;
using namespace OxideNative::Structures;

// Singleton instance
Detours* Detours::Instance;

/**
 * Macros because we're lazy
 */
#define DECLARE_DETOUR(name, returnType, ...) typedef returnType(__cdecl * name ## _Func)(__VA_ARGS__); \
name ## _Func name; \
name ## _Func name ## _original; \
returnType __cdecl Detour_ ## name(__VA_ARGS__)

#define IMPLEMENT_HOOK(name, symbolName) if (!LookupFunctionSymbol(#symbolName, (void**)&name)) \
	throw OxideNative::NativeException("Symbol for " #symbolName " not found!"); \
if (MH_CreateHook(name, & Detour_ ## name, reinterpret_cast<LPVOID*>(&name ## _original)) != MH_OK) \
	throw OxideNative::NativeException("Failed to detour " #symbolName); \
if (MH_EnableHook(name) != MH_OK) \
	throw OxideNative::NativeException("Failed to enable detour " # symbolName)

/**
 * Struct declerations
 */
struct FString
{
	int CharCount;
	int CharMax;
	const char* Pointer;
};

/**
 * Detour declarations
 */
DECLARE_DETOUR(FEngineLoop_LoadStartupCoreModules, bool, void*);
DECLARE_DETOUR(FEngineLoop_Init, int, void*);
DECLARE_DETOUR(UGameEngine_Init, void, void*, void*);
DECLARE_DETOUR(AShooterPlayerController_ServerSendChatMessage, bool, void*, FString, int);

/**
 * Initializes the Detours singleton
 */
void Detours::Initialize(SymbolMap &symbolMap, OxideNative::ulonglong imageBase)
{
	static Detours _instance(symbolMap, imageBase);
	Instance = &_instance;
}

Detours::Detours(SymbolMap &symbolMap, OxideNative::ulonglong imageBase)
	: installed(false),
	symbolMap(symbolMap),
	imageBase(imageBase)
{
	
}

Detours::~Detours()
{
	// Clean up
	if (installed)
	{
		installed = false;
	}
}

/**
 * Installs all detours
 */
void Detours::InstallHooks()
{
	// Initialize
	if (MH_Initialize() != MH_OK)
	{
		throw OxideNative::NativeException("Failed to initialise MinHook");
	}

	// Implement hooks
	IMPLEMENT_HOOK(FEngineLoop_LoadStartupCoreModules, FEngineLoop::LoadStartupCoreModules);
	IMPLEMENT_HOOK(FEngineLoop_Init, FEngineLoop::Init);
	IMPLEMENT_HOOK(UGameEngine_Init, UGameEngine::Init);
}

/**
 * Lookups the function pointer (VA) for the specified symbol
 */
bool Detours::LookupFunctionSymbol(const char *name, void **ptr, int index)
{
	OxideNative::PDB::SymbolInfo symb;
	if (!symbolMap.Find(name, index, &symb)) return false;
	*ptr = (void*)(imageBase + symb.RVA);
	return true;
}

/**
 * Detour function for FEngineLoop::LoadStartupCoreModules
 */
bool __cdecl Detour_FEngineLoop_LoadStartupCoreModules(void *engineLoop)
{
	// Call original
	bool orig = FEngineLoop_LoadStartupCoreModules_original(engineLoop);

	// Call the callback
	InjectedCore::Instance->WriteLog(LogType::Debug, "We're at LoadStartupCoreModules");
	InjectedCore::Instance->TryLoadOxide();

	// Return original
	return orig;
}

/**
* Detour function for FEngineLoop::Init
*/
int __cdecl Detour_FEngineLoop_Init(void *engineLoop)
{
	// Call hook
	InjectedCore::Instance->WriteLog(LogType::Debug, "We're at FEngineLoop::Init");
	try
	{
		OxideNative::OxideContext &oxide = InjectedCore::Instance->GetNativeContext();
		OxideNative::OxideParam param = oxide.CallHook("OnEnginePreInit", 0, nullptr);
		param.Release();
	}
	catch (OxideNative::NativeException ex)
	{
		InjectedCore::Instance->WriteLog(LogType::Error, ex.GetMessage());
	}

	// Call original
	return FEngineLoop_Init_original(engineLoop);
}

/**
 * Detour function for UGameEngine::Init
 */
void __cdecl Detour_UGameEngine_Init(void *gameEngine, void *engineLoop)
{
	// Call original
	UGameEngine_Init_original(gameEngine, engineLoop);

	// Call hook
	InjectedCore::Instance->WriteLog(LogType::Debug, "We're at UGameEngine::Init");
	try
	{
		OxideNative::OxideContext &oxide = InjectedCore::Instance->GetNativeContext();
		OxideNative::OxideParam param = oxide.CallHook("OnEnginePostInit", 0, nullptr);
		param.Release();
	}
	catch (OxideNative::NativeException ex)
	{
		InjectedCore::Instance->WriteLog(LogType::Error, ex.GetMessage());
	}
}

/**
 * Detour function for AShooterPlayerController::ServerSendChatMessage
 */
bool __cdecl Detour_AShooterPlayerController_ServerSendChatMessage(void *sender, FString message, int mode)
{
	// Call original
	AShooterPlayerController_ServerSendChatMessage_original(sender, message, mode);

	// Call hook
	InjectedCore::Instance->WriteLog(LogType::Debug, "We're at AShooterPlayerController::ServerSendChatMessage");
	try
	{
		OxideNative::OxideContext &oxide = InjectedCore::Instance->GetNativeContext();
		OxideNative::OxideParam param = oxide.CallHook("OnPlayerChat", 0, nullptr);
		param.Release();
	}
	catch (OxideNative::NativeException ex)
	{
		InjectedCore::Instance->WriteLog(LogType::Error, ex.GetMessage());
	}
}
