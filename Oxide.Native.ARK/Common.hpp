/****************\
|* Oxide Native *|
\****************/

// Common.hpp
// Holds the common definitions

#pragma once

typedef unsigned long ulong;
typedef unsigned long long ulonglong;

namespace LogType
{
	enum LogType_t
	{
		Debug,
		Info,
		Warning,
		Error
	};
};

namespace PacketType
{
	enum PacketType_t
	{
		LogMessage = 0x01,
		InjectComplete = 0x02
	};
};

struct Packet_InjectComplete
{
	ulong mainThreadId;
};