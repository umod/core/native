/****************\
|* Oxide Native *|
\****************/

// Detours.hpp
// Holds the definition for the Detours class

#pragma once

#include "../Oxide.Native/StringMap.inl"
#include "../Oxide.Native/SymbolReflector.hpp"
#include "../Oxide.Native/Types.hpp"

#include "Common.hpp"

namespace OxideNativeArk
{
	typedef OxideNative::Structures::StringMap<OxideNative::PDB::SymbolInfo> SymbolMap;

	/**
	 * Responsible for detouring functions
	 */
	class Detours
	{
	private:

		// The symbol map
		SymbolMap &symbolMap;

		// The image base
		OxideNative::ulonglong imageBase;

		// Are we installed?
		bool installed;

	public:

		// Singleton instance
		static Detours* Instance;

		/**
		 * Initializes the Detours singleton
		 */
		static void Initialize(SymbolMap &symbolMap, OxideNative::ulonglong imageBase);

	private:

		Detours(SymbolMap &symbolMap, OxideNative::ulonglong imageBase);
		~Detours();

	public:


		/**
		 * Installs all detours
		 */
		void InstallHooks();

	private:


		/**
		 * Lookups the function pointer (VA) for the specified symbol
		 */
		bool LookupFunctionSymbol(const char *name, void **ptr, int index = 0);

	};
}