/****************\
|* Oxide Native *|
\****************/

// Exports.cpp
// Holds exported functions for managed code to call into

#include "InjectedCore.hpp"

#include "../Dependencies/include/MinHook.h"

using namespace OxideNativeArk;

#define EXPORT(_type_) extern "C" __declspec(dllexport) _type_ __cdecl

EXPORT(void) OxideNative_WriteLog(LogType::LogType_t logType, const char *symbolName)
{
	InjectedCore::Instance->WriteLog(logType, symbolName);
}

EXPORT(void*) OxideNative_ReflectSymbol(const char *symbolName, int index)
{
	void *result;
	if (InjectedCore::Instance->LookupFunctionSymbol(symbolName, index, &result))
		return result;
	else
		return nullptr;
}

EXPORT(void*) OxideNative_ReverseReflectSymbol(void *symbolPtr)
{
	const char *resultName;
	if (InjectedCore::Instance->ReverseLookupSymbol(symbolPtr, &resultName))
		return (void*)resultName;
	else
		return nullptr;
}

EXPORT(void*) OxideNative_InstallHook(void *foreign, void *detour)
{
	/*void *orig;
	if (MH_CreateHook(foreign, detour, &orig) != MH_OK)
	{
		return nullptr;
	}
	if (MH_EnableHook(foreign) != MH_OK)
	{
		return nullptr;
	}
	return orig;*/
	return foreign;
}

EXPORT(void*) OxideNative_Malloc(int sz)
{
	return malloc(sz);
}

EXPORT(void) OxideNative_Free(void *ptr)
{
	free(ptr);
}