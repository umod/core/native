
#include "InjectedCore.hpp"

void BootstrapOxide()
{
	using namespace OxideNativeArk;

	// Initialize program
	InjectedCore::Instance = new InjectedCore();
	try
	{
		InjectedCore::Instance->Initialize();
	}
	catch (const char *err)
	{
		MessageBoxA(NULL, err, "Oxide Native", MB_ICONERROR);
	}
}

static DWORD CALLBACK ThreadedMain(void *ptr)
{
	BootstrapOxide();
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	using namespace OxideNativeArk;

	// Why did we get called
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		
		//CreateThread(NULL, 0, ThreadedMain, nullptr, 0, NULL);
		BootstrapOxide();

		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		
		// Shutdown program
		InjectedCore::Instance->WriteLog(LogType::Info, "Shutting down...");
		delete InjectedCore::Instance;

		break;
	}
	return TRUE;
}