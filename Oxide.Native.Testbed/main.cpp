#include <iostream>

#include "../Oxide.Native/NativeException.hpp"
#include "../Oxide.Native/OxideContext.hpp"
#include "../Oxide.Native/StringMap.inl"
#include "../Oxide.Native/SymbolReflector.hpp"

#include <time.h>

void DebugCallback(const char *message)
{
	std::cout << "Oxide: " << message << std::endl;
}

int oldmain()
{
	OxideNative::OxideContext oxContext;
	try
	{
		oxContext.SetDebugCallback(&DebugCallback);
		oxContext.Initialize();
		oxContext.WriteLog(OxideNative::LogType::Debug, "Hello world from C++!");

		OxideNative::OxideParam params[3];
		params[0] = OxideNative::OxideParam::Variant(10);
		params[1] = OxideNative::OxideParam::Null();
		params[2] = OxideNative::OxideParam::Variant(L"Hello world!");

		OxideNative::OxideParam returnVal = oxContext.CallHook("TestHook", 3, params);
		params[0].Release();
		params[1].Release();
		params[2].Release();

		if (returnVal.GetType() == OxideNative::ParamType::Variant)
		{
			OxideNative::OxideVariant v;
			memcpy(&v, returnVal.GetData(), sizeof(OxideNative::OxideVariant));
			if (v.vt == OxideNative::VariantType::VT_BSTR)
			{
				std::cout << "Got a string in reply!" << std::endl;
				std::wcout << v.bstrVal << std::endl;
			}
		}
		returnVal.Release();
	}
	catch (OxideNative::NativeException &ex)
	{
		std::cout << ex.GetMessage() << std::endl;
		system("pause");
	}

	std::cout << "All seems good." << std::endl;
	system("pause");
	return 0;
}

int unitmain()
{
	OxideNative::Structures::StringMap<int> myStringMap;

	myStringMap.Insert("Test1", 1);
	myStringMap.Insert("Hello", 2);
	myStringMap.Insert("Hi", 3);
	myStringMap.Insert("HelloWorld", 4);
	myStringMap.Insert("HelloWo", 5);
	myStringMap.Insert("Hello", 6);
	myStringMap.Insert("Test2", 7);
	myStringMap.Insert("MyClass", 8);
	myStringMap.Insert("MyClass_Thing1", 17);
	myStringMap.Insert("MyClass_Thing2", 9);
	myStringMap.Insert("MyClass_OtherThing", 10);
	myStringMap.Insert("MyClass_Blarg", 11);
	myStringMap.Insert("MyClass_Lolzorz", 12);
	myStringMap.Insert("MyClass_YetAnotherThing", 13);
	myStringMap.Insert("MyClass_Moar", 14);
	myStringMap.Insert("MyClass_EvenMoar", 15);
	myStringMap.Insert("MyClass_Another", 16);

	int testVal;

	// Search for the keys we inserted
	if (!myStringMap.Find("Test1", 0, &testVal) || testVal != 1)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("Hello", 0, &testVal) || testVal != 2)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("Hi", 0, &testVal) || testVal != 3)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("HelloWorld", 0, &testVal) || testVal != 4)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("HelloWo", 0, &testVal) || testVal != 5)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("Hello", 1, &testVal) || testVal != 6)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("Test2", 0, &testVal) || testVal != 7)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass", 0, &testVal) || testVal != 8)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass_Thing1", 0, &testVal) || testVal != 17)
	{
		throw "Failed";
	}

	if (!myStringMap.Find("MyClass_Thing2", 0, &testVal) || testVal != 9)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass_OtherThing", 0, &testVal) || testVal != 10)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass_Blarg", 0, &testVal) || testVal != 11)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass_Lolzorz", 0, &testVal) || testVal != 12)
	{
		throw "Failed";
	}
	if (!myStringMap.Find("MyClass_YetAnotherThing", 0, &testVal) || testVal != 13)
	{
		throw "Failed";
	}

	// Search for keys we DIDN'T insert
	if (myStringMap.Find("Test3", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("Hell", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("Helloo", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("H", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("HelloW", 0, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("Hello", 2, &testVal))
	{
		throw "Failed";
	}
	if (myStringMap.Find("MyClass_", 0, &testVal))
	{
		throw "Failed";
	}

	return 0;
}

int main()
{
	using namespace OxideNative;
	using namespace OxideNative::PDB;
	using namespace OxideNative::Structures;

	StringMap<ulonglong> myStringMap;

	SymbolReflector symRef;
	symRef.Load("E:\\Development\\Oxide\\ark server\\ShooterGame\\Binaries\\Win64\\ShooterGameServer.exe");

	SymbolIterator lambda = [](const char *name, SymbolInfo &symInfo, void *userData)
	{
		StringMap<ulonglong> *myStringMap = (StringMap<ulonglong>*)userData;
		try
		{
			myStringMap->Insert(name, symInfo.RVA);
		}
		catch (const char *ex)
		{
			std::cout << ex << std::endl;
		}
	};
	symRef.Enumerate(lambda, &myStringMap);

	StringMapMetrics metrics = myStringMap.GetMetrics();
	std::cout << "StringMap is using " << metrics.TableCount << " tables and " << metrics.ValueCount << " values" << std::endl;
	std::cout << "There are " << metrics.DuplicateCount << " duplicate symbols" << std::endl;

	std::cout << "There are " << metrics.TotalValueChains << " value chains (more = slower lookups!)" << std::endl;
	std::cout << "There are " << metrics.TotalTableRootChainHeads << " table root value chains" << std::endl;
	std::cout << "The largest table root value chain is " << metrics.HighestTableRootChain << " values big" << std::endl;

	int totalMem = metrics.CharMemUsage + metrics.TableMemUsage + metrics.ValueMemUsage;
	std::cout << "Memory usage: " << totalMem << " bytes" << std::endl;

	float charProp = metrics.CharMemUsage / (float)totalMem;
	std::cout << "Strings take up " << (charProp * 100.0f) << "% of the total memory usage" << std::endl;

	float tablesProp = metrics.TableMemUsage / (float)totalMem;
	std::cout << "Tables take up " << (tablesProp * 100.0f) << "% of the total memory usage" << std::endl;

	float valuesProp = metrics.ValueMemUsage / (float)totalMem;
	std::cout << "Values take up " << (valuesProp * 100.0f) << "% of the total memory usage" << std::endl;

	clock_t cBefore = clock();
	SymbolIterator lambda2 = [](const char *name, SymbolInfo &symInfo, void *userData)
	{
		StringMap<ulonglong> *myStringMap = (StringMap<ulonglong>*)userData;
		ulonglong rva;
		if (!myStringMap->Find(name, 0, &rva))
		{
			std::cout << "Lookup failed on " << name << std::endl;
		}
	};
	symRef.Enumerate(lambda2, &myStringMap);

	clock_t cAfter = clock();

	long diff = cAfter - cBefore;
	double diffPerValue = diff / (double)metrics.ValueCount;

	std::cout << "Average lookup time: " << diffPerValue << "ms per symbol" << std::endl;

	cBefore = clock();
	SymbolIterator lambda3 = [](const char *name, SymbolInfo &symInfo, void *userData)
	{
		StringMap<ulonglong> *myStringMap = (StringMap<ulonglong>*)userData;
		const char *lookupname = myStringMap->ReverseLookup(symInfo.RVA);
		if (strcmp(lookupname, name) != 0)
		{
			std::cout << "Reverse lookup failed on " << name << std::endl;
		}
	};
	symRef.Enumerate(lambda3, &myStringMap);

	cAfter = clock();

	diff = cAfter - cBefore;
	diffPerValue = diff / (double)metrics.ValueCount;

	std::cout << "Average reverse lookup time: " << diffPerValue << "ms per symbol" << std::endl;

	return 0;
}